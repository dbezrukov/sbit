#ifndef SBITAPI_H
#define SBITAPI_H

#include <QObject>
#include <QTimer>
#include <QMap>

#include "core/common.h"
#include "core/tickersmodel.h"
#include "core/ordersmodel.h"
#include "core/networkmanager.h"
#include "core/depthmodel.h"
#include "core/trade.h"
#include "core/chartsmodel.h"

class SBitApi : public QObject
{
    Q_OBJECT

public:
    static SBitApi* instance();

    // Set account
    void setCredentials(QString, QString);

    // Account
    Funds funds() { return m_funds; }
    TickersModel* tickers() { return m_tickersModel; }

    // Orders
    OrdersModel* orders() { return m_ordersModel; }

    // Trade history
    OrdersModel* ordersHistory() { return m_ordersHistoryModel; }

    // Depth
    DepthModel* depthModel(const QString& pair);

    // Chart
    ChartsModel* chartModel(const QString& pair);


    void placeBuyOrder(QString tickerName, double amount, double rate,
                       QObject* observer = 0, const char* successSlot = 0, const char* errorSlot = 0);
    void placeSellOrder(QString tickerName, double amount, double rate,
                       QObject* observer = 0, const char* successSlot = 0, const char* errorSlot = 0);
    void cancelOrder(QString orderId, QObject* observer = 0, const char* successSlot = 0, const char* errorSlot = 0);

protected:
    SBitApi(QObject* parent = 0);
    virtual ~SBitApi() {}
    void setupTickers();

protected slots:
    void orderOperationSuccess(Funds);

    void queryAccountInfo();
    void queryAccountInfoSuccess(Funds);
    void queryAccountInfoError(QString);

    void queryTickerInfo();
    void queryTickerInfoSuccess(Ticker*);
    void queryTickerInfoError(QString);

    void queryTradeInfo();
    void queryTradeInfoSuccess(QString, QMap<qlonglong, Trade*>);
    void queryTradeInfoError(QString);

    void queryDepthInfo();
    void queryDepthInfoSuccess(QString, QList<DepthOffer*>);
    void queryDepthInfoError(QString);

    void queryTradeHistory();
    void queryTradeHistorySuccess(QList<Order*>);
    void queryTradeHistoryError(QString);

    void queryTransactHistory();
    void queryTransactHistorySuccess(Ticker*);
    void queryTransactHistoryError(QString);

    void queryOrders();
    void queryOrdersSuccess(QList<Order*>);
    void queryOrdersError(QString);

    void queryChartsData();
    void queryChartsDataSuccess(QString, QList<OhlcData*>);
    void queryChartsDataError(QString);

signals:
    void accountInfoUpdated();
    void tickersInfoUpdated();

    void connectionFailed();

private:
    static SBitApi* m_instance;
    NetworkManager* m_networkManager;

    QString m_tradeControllerUrl;

    QTimer* m_tickersUpdateTimer;
    QTimer* m_accountInfoUpdateTimer;
    QTimer* m_tradeInfoUpdateTimer;
    QTimer* m_chartsDataUpdateTimer;

    Funds m_funds;

    TickersModel* m_tickersModel;
    OrdersModel* m_ordersModel;
    OrdersModel* m_ordersHistoryModel;
    QMap<QString, DepthModel*> m_depthModels;
    QMap<QString, ChartsModel*> m_chartsModels;

    bool m_accountInfoReceived;

    Q_DISABLE_COPY(SBitApi)
};

#endif // SBITAPI_H
