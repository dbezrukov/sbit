#include "tickerinforequest.h"
#include "qt-json/json.h"

#include <QDebug>

TickerInfoRequest::TickerInfoRequest(QString name, const QString& controllerUrl, NetworkManager* networkManager)
    : AbstractRequest(controllerUrl, QString(), RequestArgs(), networkManager, false)
    , m_name(name)
{
}

void TickerInfoRequest::readyRead()
{
    QByteArray data = m_reply->readAll();

    //qDebug() << Q_FUNC_INFO << data;

    bool ok;
    QVariantMap result = QtJson::parse(data, ok).toMap();

    if(ok) {
        // "ticker":{"high":114.012,"low":110.86,"avg":112.436,"vol":513709.04817,
        //           "vol_cur":4558.83187,"last":112.464,"buy":112.89,"sell":112.5,"server_time":1368505262}

        QVariantMap tickerInfo = result["ticker"].toMap();

        Ticker* ticker = new Ticker(m_name);
        ticker->setPrice(tickerInfo["buy"].toDouble());

        emit done(ticker);
    }
}
