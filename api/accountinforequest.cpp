#include "accountinforequest.h"
#include "qt-json/json.h"
#include "core/networkmanager.h"

#include <QDebug>

AccountInfoRequest::AccountInfoRequest(const QString& controllerUrl, NetworkManager* networkManager)
    : AbstractRequest(controllerUrl, "getInfo", RequestArgs(), networkManager)
    , m_networkManager(networkManager)
{
}

void AccountInfoRequest::readyRead()
{
    QByteArray data = m_reply->readAll();

    qDebug() << Q_FUNC_INFO << data;

    bool ok;
    QVariantMap result = QtJson::parse(data, ok).toMap();

    if(ok) {
        bool success = result["success"].toBool();
        if (!success) {
            qDebug() << Q_FUNC_INFO << ", error: " << result["error"];

            QString errorString = result["error"].toString();
            if (errorString.contains("invalid nonce parameter")) {
                QString currentNonce = errorString.section(' ', -2, -2);
                qDebug() << "current nonce is: " << currentNonce;
                m_networkManager->setNonce(currentNonce.toLongLong());
            }
            emit failed(errorString);
            return;
        }

        QVariantMap returned = result["return"].toMap();
        QVariantMap returnedFunds = returned["funds"].toMap();

        Funds funds = fundsFromJson(returnedFunds);
        emit done(funds);
    }
}
