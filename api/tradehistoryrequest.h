#ifndef TRADEHISTORYREQUEST_H
#define TRADEHISTORYREQUEST_H

#include "abstractrequest.h"
#include "core/common.h"
#include "core/order.h"

// Buy/sell history
class TradeHistoryRequest : public AbstractRequest
{
    Q_OBJECT

public:
    explicit TradeHistoryRequest(const QString& controllerUrl, NetworkManager* networkManager);

signals:
    void done(QList<Order*>);

protected slots:
    virtual void readyRead();
};


#endif // TRADEHISTORYREQUEST_H
