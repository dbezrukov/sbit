#include "cancelorderrequest.h"
#include "qt-json/json.h"

#include <QDebug>

CancelOrderRequest::CancelOrderRequest(const QString& orderId, const QString& controllerUrl,
                                       NetworkManager* networkManager)
    : AbstractRequest(controllerUrl, "CancelOrder", constructArgs(orderId), networkManager)
{
}

void CancelOrderRequest::readyRead()
{
    QByteArray data = m_reply->readAll();

    //qDebug() << Q_FUNC_INFO << data;

    bool ok;
    QVariantMap result = QtJson::parse(data, ok).toMap();

    if(ok) {
        bool success = result["success"].toBool();
        if (!success) {
            qDebug() << Q_FUNC_INFO << ", error: " << result["error"];
            emit failed(result["error"].toString());
            return;
        }

        QVariantMap returned = result["return"].toMap();
        QVariantMap returnedFunds = returned["funds"].toMap();

        Funds funds = fundsFromJson(returnedFunds);
        emit done(funds);
    }
}

RequestArgs CancelOrderRequest::constructArgs(const QString& orderId)
{
    return RequestArgs() << qMakePair(QString("order_id"), orderId);
}
