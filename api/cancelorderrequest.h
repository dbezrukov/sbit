#ifndef CANCELORDERREQUEST_H
#define CANCELORDERREQUEST_H

#include "abstractrequest.h"
#include "core/common.h"
#include "core/order.h"

class CancelOrderRequest : public AbstractRequest
{
    Q_OBJECT

public:
    explicit CancelOrderRequest(const QString& orderId, const QString& controllerUrl, NetworkManager* networkManager);
signals:
    void done(Funds);

protected slots:
    virtual void readyRead();

protected:
    RequestArgs constructArgs(const QString& orderId);
};

#endif // CANCELORDERREQUEST_H
