#ifndef ORDERSREQUEST_H
#define ORDERSREQUEST_H

#include "abstractrequest.h"
#include "core/common.h"
#include "core/order.h"

class OrdersRequest : public AbstractRequest
{
    Q_OBJECT

public:
    explicit OrdersRequest(const QString& controllerUrl, NetworkManager* networkManager);

signals:
    void done(QList<Order*>);

protected slots:
    virtual void readyRead();
};

#endif // ORDERSREQUEST_H
