#include "tradeinforequest.h"
#include "qt-json/json.h"

#include <QDebug>

TradeInfoRequest::TradeInfoRequest(QString name, const QString& controllerUrl, NetworkManager* networkManager)
    : AbstractRequest(controllerUrl, QString(), RequestArgs(), networkManager, false)
    , m_name(name)
{
}

void TradeInfoRequest::readyRead()
{
    QByteArray data = m_reply->readAll();

    //qDebug() << Q_FUNC_INFO << data;

    bool ok;
    QVariantList result = QtJson::parse(data, ok).toList();

    QMap<qlonglong, Trade*> trades;

    if(ok) {
        // [{"date":1372221128,"price":0.02725,"amount":0.25,"tid":5451845,"price_currency":"BTC","item":"LTC","trade_type":"bid"},
        //  {"date":1372221120,"price":0.02724,"amount":1.21112,"tid":5451844,"price_currency":"BTC","item":"LTC","trade_type":"ask"}]

        //qDebug() << Q_FUNC_INFO << ", get trades count: " << result.length();

        foreach (QVariant tradeResult, result) {

            QVariantMap tradeInfo = tradeResult.toMap();
            //qDebug() << Q_FUNC_INFO << tradeInfo;

            qlonglong tid = tradeInfo["tid"].toLongLong();
            qlonglong date = tradeInfo["date"].toLongLong();
            double price = tradeInfo["price"].toDouble();
            double amount = tradeInfo["amount"].toDouble();
            QString trade_type = tradeInfo["trade_type"].toString();

            Trade* trade = new Trade(tid, date);
            trade->setPrice(price);
            trade->setAmount(amount);
            trade->setSell(trade_type == "bid" ? true : false);

            trades.insert(trade->id(), trade);
        }

        emit done(m_name, trades);
    }
}
