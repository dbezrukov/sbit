#include "abstractrequest.h"

#include <QDebug>

#include "core/networkmanager.h"

AbstractRequest::AbstractRequest(const QString& controllerUrl,
                                 const QString& method,
                                 const RequestArgs& args,
                                 NetworkManager* networkManager,
                                 bool shouldQueue)
    : QObject(networkManager)
    , m_reply(0)
{
    setUrl(controllerUrl);
    setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded"));

    QString requestString;

    // set method name
    if (!method.isEmpty()) {
        requestString = QString("method=%1").arg(method);
    }

    // set additional args
    foreach (RequestArg arg, args) {
        requestString += QString("&%1=%2").arg(arg.first).arg(arg.second);
    }

    if (shouldQueue) {
        networkManager->enqueue(this, requestString);
    } else {
        networkManager->sendRequest(this, requestString);
    }
}

void AbstractRequest::setReply(QNetworkReply *reply)
{
    m_reply = reply;

    connect(m_reply, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(m_reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
    connect(m_reply, SIGNAL(finished()), this, SLOT(finished()));
}

void AbstractRequest::error(QNetworkReply::NetworkError)
{
    emit failed(QString("Service not found, please check the app config"));
}

void AbstractRequest::finished()
{
    m_reply->deleteLater();
    delete this;
}
