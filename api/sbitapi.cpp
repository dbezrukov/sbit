#include "sbitapi.h"
#include "api/accountinforequest.h"
#include "api/tickerinforequest.h"
#include "api/tradehistoryrequest.h"
#include "api/tradeinforequest.h"
#include "api/depthinforequest.h"
#include "api/transacthistoryrequest.h"
#include "api/ordersrequest.h"
#include "api/buyrequest.h"
#include "api/sellrequest.h"
#include "api/cancelorderrequest.h"
#include "api/chartdatarequest.h"

#include <QFile>
#include <QDebug>
#include <QMessageBox>
#include <QCoreApplication>

SBitApi* SBitApi::m_instance = NULL;

SBitApi::SBitApi(QObject *parent)
    : QObject(parent)
    , m_networkManager(0)
    , m_tradeControllerUrl("https://btc-e.com/tapi")
    , m_tickersUpdateTimer(new QTimer(this))
    , m_accountInfoUpdateTimer(new QTimer(this))
    , m_tradeInfoUpdateTimer(new QTimer(this))
    , m_chartsDataUpdateTimer(new QTimer(this))
    , m_tickersModel(new TickersModel(this))
    , m_ordersModel(new OrdersModel(this))
    , m_ordersHistoryModel(new OrdersModel(this))
    , m_accountInfoReceived(false)
{
    setupTickers();
}

void SBitApi::setupTickers()
{
    foreach (QString pair, TickerNames::list) {
        m_tickersModel->append(new Ticker(pair, this));
        m_depthModels.insert(pair, new DepthModel(this));
        m_chartsModels.insert(pair, new ChartsModel(this));
    }
}

void SBitApi::orderOperationSuccess(Funds funds)
{
    queryAccountInfoSuccess(funds);
    queryOrders();
}

SBitApi* SBitApi::instance()
{
    if (m_instance == NULL) {
        m_instance = new SBitApi();
    }
    return m_instance;
}

void SBitApi::setCredentials(QString key, QString secret)
{
    if (key.isEmpty() || secret.isEmpty())
        return;

    m_networkManager = new NetworkManager(key, secret, this);

    connect(m_tickersUpdateTimer, SIGNAL(timeout()), this, SLOT(queryTickerInfo()));
    queryTickerInfo();

    connect(m_tickersUpdateTimer, SIGNAL(timeout()), this, SLOT(queryDepthInfo()));
    queryDepthInfo();

    connect(m_accountInfoUpdateTimer, SIGNAL(timeout()), this, SLOT(queryAccountInfo()));
    queryAccountInfo();

    connect(m_chartsDataUpdateTimer, SIGNAL(timeout()), this, SLOT(queryChartsData()));
    queryChartsData();

    m_tickersUpdateTimer->start(5000);
    m_accountInfoUpdateTimer->start(8000);
    m_chartsDataUpdateTimer->start(60000);
}

DepthModel *SBitApi::depthModel(const QString &pair)
{
    return m_depthModels[pair];
}

ChartsModel *SBitApi::chartModel(const QString& pair)
{
    return m_chartsModels[pair];
}

void SBitApi::placeBuyOrder(QString tickerName, double amount, double rate,
                            QObject *observer, const char *successSlot, const char *errorSlot)
{
    BuyRequest* request;
    request = new BuyRequest(tickerName, amount, rate, m_tradeControllerUrl, m_networkManager);
    connect(request, SIGNAL(done(Funds)), this, SLOT(orderOperationSuccess(Funds)));
    connect(request, SIGNAL(failed(QString)), this, SLOT(queryAccountInfoError(QString)));
}

void SBitApi::placeSellOrder(QString tickerName, double amount, double rate, QObject *observer, const char *successSlot, const char *errorSlot)
{
    SellRequest* request;
    request = new SellRequest(tickerName, amount, rate, m_tradeControllerUrl, m_networkManager);
    connect(request, SIGNAL(done(Funds)), this, SLOT(orderOperationSuccess(Funds)));
    connect(request, SIGNAL(failed(QString)), this, SLOT(queryAccountInfoError(QString)));
}

void SBitApi::cancelOrder(QString orderId, QObject *observer, const char *successSlot, const char *errorSlot)
{
    CancelOrderRequest* request;
    request = new CancelOrderRequest(orderId, m_tradeControllerUrl, m_networkManager);
    connect(request, SIGNAL(done(Funds)), this, SLOT(orderOperationSuccess(Funds)));
    connect(request, SIGNAL(failed(QString)), this, SLOT(queryAccountInfoError(QString)));
}

void SBitApi::queryAccountInfo()
{
    AccountInfoRequest* accountInfoRequest;
    accountInfoRequest = new AccountInfoRequest(m_tradeControllerUrl, m_networkManager);
    connect(accountInfoRequest, SIGNAL(done(Funds)), this, SLOT(queryAccountInfoSuccess(Funds)));
    connect(accountInfoRequest, SIGNAL(failed(QString)), this, SLOT(queryAccountInfoError(QString)));
}

void SBitApi::queryAccountInfoSuccess(Funds accountFunds)
{
    m_funds = accountFunds;
    emit accountInfoUpdated();

    // continue using private API with given permissions
    if (!m_accountInfoReceived) {
        m_accountInfoReceived = true;

        // get trade info
        connect(m_tradeInfoUpdateTimer, SIGNAL(timeout()), this, SLOT(queryOrders()));
        queryOrders();

        connect(m_tradeInfoUpdateTimer, SIGNAL(timeout()), this, SLOT(queryTradeHistory()));
        queryTradeHistory();

        //connect(m_updateTimer, SIGNAL(timeout()), this, SLOT(queryTransactHistory()));
        //queryTransactHistory();

        m_tradeInfoUpdateTimer->start(10000);
    }
}

void SBitApi::queryAccountInfoError(QString)
{
    emit connectionFailed();
}

void SBitApi::queryTickerInfo()
{
    foreach (QString ticker, TickerNames::list) {
        QString pair = ticker;
        pair = pair.replace('/', "_").toLower();

        TickerInfoRequest* tickerInfoRequest;
        tickerInfoRequest = new TickerInfoRequest(ticker, QString("https://btc-e.com/api/2/%1/ticker").arg(pair), m_networkManager);
        connect(tickerInfoRequest, SIGNAL(done(Ticker*)), this, SLOT(queryTickerInfoSuccess(Ticker*)));
        connect(tickerInfoRequest, SIGNAL(failed(QString)), this, SLOT(queryTickerInfoError(QString)));
    }
}

void SBitApi::queryTickerInfoSuccess(Ticker* updatedTicker)
{
    Ticker* ticker = m_tickersModel->item(updatedTicker->name());
    if (ticker) {
        ticker->setPrice(updatedTicker->price());
        emit tickersInfoUpdated();
    } else {
        qDebug() << "Ticker " << updatedTicker->name() << " not found";
    }
    updatedTicker->deleteLater();
}

void SBitApi::queryTickerInfoError(QString)
{
    emit connectionFailed();
}

void SBitApi::queryTradeInfo()
{
    foreach (QString ticker, TickerNames::list) {
        QString pair = ticker;
        pair = pair.replace('/', "_").toLower();

        TradeInfoRequest* tradeInfoRequest;
        tradeInfoRequest = new TradeInfoRequest(ticker, QString("https://btc-e.com/api/2/%1/trades").arg(pair), m_networkManager);
        connect(tradeInfoRequest, SIGNAL(done(QString, QMap<qlonglong, Trade*>)),
                this, SLOT(queryTradeInfoSuccess(QString, QMap<qlonglong, Trade*>)));
        connect(tradeInfoRequest, SIGNAL(failed(QString)), this, SLOT(queryTradeInfoError(QString)));
    }
}

void SBitApi::queryTradeInfoSuccess(QString tickerName, QMap<qlonglong, Trade*> trades)
{
    /*
    DepthModel* tradesModel = m_depthModels[tickerName];
    if (!tradesModel) {
        qDebug() << "Trade model for ticker " << tickerName << " not found";
        return;
    }

    tradesModel->append(trades);
    */
}

void SBitApi::queryTradeInfoError(QString)
{
    emit connectionFailed();
}

void SBitApi::queryDepthInfo()
{
    foreach (QString ticker, TickerNames::list) {
        QString pair = ticker;
        pair = pair.replace('/', "_").toLower();

        DepthInfoRequest* depthInfoRequest;
        depthInfoRequest = new DepthInfoRequest(ticker, QString("https://btc-e.com/api/2/%1/depth").arg(pair), m_networkManager);
        connect(depthInfoRequest, SIGNAL(done(QString,QList<DepthOffer*>)),
                this, SLOT(queryDepthInfoSuccess(QString,QList<DepthOffer*>)));
        connect(depthInfoRequest, SIGNAL(failed(QString)), this, SLOT(queryDepthInfoError(QString)));
    }
}

void SBitApi::queryDepthInfoSuccess(QString tickerName, QList<DepthOffer*> depth)
{
    DepthModel* depthModel = m_depthModels[tickerName];
    if (!depthModel) {
        qDebug() << "Depth model for ticker " << tickerName << " not found";
        return;
    }

    depthModel->setDepth(depth);
}

void SBitApi::queryDepthInfoError(QString)
{
    emit connectionFailed();
}

void SBitApi::queryTradeHistory()
{
    TradeHistoryRequest* request;
    request = new TradeHistoryRequest(m_tradeControllerUrl, m_networkManager);
    connect(request, SIGNAL(done(QList<Order*>)), this, SLOT(queryTradeHistorySuccess(QList<Order*>)));
    connect(request, SIGNAL(failed(QString)), this, SLOT(queryTradeHistoryError(QString)));
}

void SBitApi::queryTradeHistorySuccess(QList<Order*> orders)
{
    foreach (Order* updatedOrder, orders)
    {
        Order* order = m_ordersHistoryModel->item(updatedOrder->id());

        if (order) {
            order->setStatus(updatedOrder->status());
        } else {
            m_ordersHistoryModel->append(updatedOrder);
        }
    }
}

void SBitApi::queryTradeHistoryError(QString)
{
    emit connectionFailed();
}

void SBitApi::queryTransactHistory()
{
    TransactHistoryRequest* request;
    request = new TransactHistoryRequest(m_tradeControllerUrl, m_networkManager);
    connect(request, SIGNAL(done(Ticker*)), this, SLOT(queryTransactHistorySuccess(Ticker*)));
    connect(request, SIGNAL(failed(QString)), this, SLOT(queryTransactHistoryError(QString)));
}

void SBitApi::queryTransactHistorySuccess(Ticker *)
{
}

void SBitApi::queryTransactHistoryError(QString)
{
    emit connectionFailed();
}

void SBitApi::queryOrders()
{
    OrdersRequest* request;
    request = new OrdersRequest(m_tradeControllerUrl, m_networkManager);
    connect(request, SIGNAL(done(QList<Order*>)), this, SLOT(queryOrdersSuccess(QList<Order*>)));
    connect(request, SIGNAL(failed(QString)), this, SLOT(queryOrdersError(QString)));
}

void SBitApi::queryOrdersSuccess(QList<Order*> orders)
{
    foreach (Order* updatedOrder, orders)
    {
        Order* order = m_ordersModel->item(updatedOrder->id());

        if (order) {
            if (updatedOrder->status() > 0) {
                m_ordersModel->remove(order);
            }
        } else if (updatedOrder->status() == 0) {
            m_ordersModel->append(updatedOrder);
        }
    }
}

void SBitApi::queryOrdersError(QString)
{
    emit connectionFailed();
}

void SBitApi::queryChartsData()
{
    foreach (QString ticker, TickerNames::list) {
        int chartId = 0;

        if(ticker.compare(TickerNames::btcUsd) == 0)
            chartId = 1005;
        else if(ticker.compare(TickerNames::btcRur) == 0)
            chartId = 1010;
        else if(ticker.compare(TickerNames::btcEur) == 0)
            chartId = 1015;
        else if(ticker.compare(TickerNames::ltcBtc) == 0)
            chartId = 1020;
        else if(ticker.compare(TickerNames::ltcUsd) == 0)
            chartId = 1025;

        ChartDataRequest* chartDataRequest;
        chartDataRequest = new ChartDataRequest(ticker, QString("http://www.gobitgo.com/_inc/grabChartData.php?chartId=%1").arg(chartId), m_networkManager);
        connect(chartDataRequest, SIGNAL(done(QString, QList<OhlcData*>)),
                this, SLOT(queryChartsDataSuccess(QString, QList<OhlcData*>)));
        connect(chartDataRequest, SIGNAL(failed(QString)), this, SLOT(queryChartsDataError(QString)));
    }

}

void SBitApi::queryChartsDataSuccess(QString tickerName, QList<OhlcData*> prices)
{
    ChartsModel* chartsModel = m_chartsModels[tickerName];
    if (!chartsModel) {
        qDebug() << "Chart model for ticker " << tickerName << " not found";
        return;
    }

    foreach (OhlcData* updatedOhlcData, prices) {
        OhlcData* ohlcData = chartsModel->item(updatedOhlcData->timestamp());

        if (ohlcData) {
            chartsModel->remove(ohlcData);
            chartsModel->append(updatedOhlcData);
        } else {
            chartsModel->append(updatedOhlcData);
        }
    }
}

void SBitApi::queryChartsDataError(QString)
{
    emit connectionFailed();
}
