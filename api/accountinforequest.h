#ifndef ACCOUNTINFOREQUEST_H
#define ACCOUNTINFOREQUEST_H

#include "abstractrequest.h"
#include "core/common.h"

class AccountInfoRequest : public AbstractRequest
{
    Q_OBJECT

public:
    explicit AccountInfoRequest(const QString& controllerUrl, NetworkManager* networkManager);

signals:
    void done(Funds);

protected slots:
    virtual void readyRead();

private:
    NetworkManager* m_networkManager;
};

#endif // ACCOUNTINFOREQUEST_H
