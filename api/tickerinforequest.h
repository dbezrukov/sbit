#ifndef TICKERINFOREQUEST_H
#define TICKERINFOREQUEST_H

#include "abstractrequest.h"
#include "core/common.h"
#include "core/ticker.h"

class TickerInfoRequest : public AbstractRequest
{
    Q_OBJECT

public:
    explicit TickerInfoRequest(QString name, const QString& controllerUrl, NetworkManager* networkManager);

signals:
    void done(Ticker*);

protected slots:
    virtual void readyRead();

protected:
    QString m_name;
};

#endif // TICKERINFOREQUEST_H
