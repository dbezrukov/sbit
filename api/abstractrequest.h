#ifndef ABSTRACTREQUEST_H
#define ABSTRACTREQUEST_H

#include <QObject>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QPair>
#include <QList>

class NetworkManager;

typedef QPair<QString, QString> RequestArg;
typedef QList<RequestArg> RequestArgs;

class AbstractRequest : public QObject, public QNetworkRequest
{
    Q_OBJECT

public:
    explicit AbstractRequest(const QString& controllerUrl, const QString& method,
                             const RequestArgs& args, NetworkManager* networkManager,
                             bool shouldQueue = true);
    void setReply(QNetworkReply*);

signals:
    void failed(QString error);

protected slots:
    virtual void readyRead() = 0;
    void error(QNetworkReply::NetworkError code);
    void finished();

protected:
    QNetworkReply* m_reply;
};

#endif // ABSTRACTREQUEST_H
