#ifndef SELLREQUEST_H
#define SELLREQUEST_H

#include "abstractrequest.h"
#include "core/common.h"
#include "core/order.h"

class SellRequest : public AbstractRequest
{
    Q_OBJECT

public:
    explicit SellRequest(QString tickerName, double amount, double rate,
                         const QString& controllerUrl, NetworkManager* networkManager);

signals:
    void done(Funds);

protected slots:
    virtual void readyRead();

protected:
    RequestArgs constructArgs(QString tickerName, double amount, double rate);
};

#endif // SELLREQUEST_H
