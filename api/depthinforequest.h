#ifndef DEPTHINFOREQUEST_H
#define DEPTHINFOREQUEST_H

#include <QObject>
#include "abstractrequest.h"
#include "core/common.h"
#include "core/depthoffer.h"

class DepthInfoRequest : public AbstractRequest
{
    Q_OBJECT

public:
    explicit DepthInfoRequest(QString name, const QString& controllerUrl, NetworkManager* networkManager);
    
signals:
    void done(QString tickerName, QList<DepthOffer*> depth);

protected slots:
    virtual void readyRead();

protected:
    QString m_name;
};

#endif // DEPTHINFOREQUEST_H
