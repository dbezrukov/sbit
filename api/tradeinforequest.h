#ifndef TRADEINFOREQUEST_H
#define TRADEINFOREQUEST_H

#include "abstractrequest.h"
#include "core/common.h"
#include "core/trade.h"

class TradeInfoRequest : public AbstractRequest
{
    Q_OBJECT

public:
    explicit TradeInfoRequest(QString name, const QString& controllerUrl, NetworkManager* networkManager);

signals:
    void done(QString tickerName, QMap<qlonglong, Trade*> trades);

protected slots:
    virtual void readyRead();

protected:
    QString m_name;
};


#endif // TRADEINFOREQUEST_H
