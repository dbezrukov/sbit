#include "depthinforequest.h"

#include "qt-json/json.h"

DepthInfoRequest::DepthInfoRequest(QString name, const QString &controllerUrl, NetworkManager *networkManager)
    : AbstractRequest(controllerUrl, QString(), RequestArgs(), networkManager, false)
    , m_name(name)
{
}


void DepthInfoRequest::readyRead()
{
    QByteArray data = m_reply->readAll();

    //qDebug() << Q_FUNC_INFO << data;

    bool ok;
    QVariantMap depthMap = QtJson::parse(data, ok).toMap();

    QList<DepthOffer*> depth;

    if (ok)
    {
        // {"asks":[[85.478,0.44212693],[85.479,0.05],...
        // ,"bids":[[85.1,16.98016324],[85.008,10.5891197],

        // TODO : add checks if asks and bids exist, and emiting some kind of error.
        QVariantList asksList = depthMap["asks"].toList();
        QVariantList bidsList = depthMap["bids"].toList();

        foreach (QVariant depthOfferAsk, asksList)
        {
            QVariantList askOffer = depthOfferAsk.toList();

            DepthOffer* pDepthOffer = new DepthOffer(); // TODO : solve memory leaks issues
            pDepthOffer->setSell(false);
            pDepthOffer->setPrice(askOffer[0].toDouble());
            pDepthOffer->setAmount(askOffer[1].toDouble());

            depth.push_back(pDepthOffer);
        }

        foreach (QVariant depthOfferAsk, bidsList)
        {
            QVariantList bidsOffer = depthOfferAsk.toList();

            DepthOffer* pDepthOffer = new DepthOffer(); // TODO : solve memory leaks issues
            pDepthOffer->setSell(true);
            pDepthOffer->setPrice(bidsOffer[0].toDouble());
            pDepthOffer->setAmount(bidsOffer[1].toDouble());

            depth.push_back(pDepthOffer);
        }

        emit done(m_name, depth);
    }
}

