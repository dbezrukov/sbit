#ifndef CHARTDATAREQUEST_H
#define CHARTDATAREQUEST_H

#include "abstractrequest.h"
#include "core/common.h"
#include "core/ohlcdata.h"

class ChartDataRequest : public AbstractRequest
{
    Q_OBJECT

public:
    explicit ChartDataRequest(QString name, const QString& controllerUrl, NetworkManager* networkManager);

signals:
    void done(QString pairName, QList<OhlcData*> prices);
    //void done(QString pairName, QMap<qlonglong, OhlcData*> prices);

protected slots:
    virtual void readyRead();

protected:
    QString m_name;
};

#endif // CHARTDATAREQUEST_H
