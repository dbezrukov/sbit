#include "sellrequest.h"
#include "qt-json/json.h"

#include <QDebug>

SellRequest::SellRequest(QString tickerName, double amount, double rate,
                         const QString& controllerUrl, NetworkManager* networkManager)
    : AbstractRequest(controllerUrl, "Trade", constructArgs(tickerName, amount, rate), networkManager)
{
}

void SellRequest::readyRead()
{
    QByteArray data = m_reply->readAll();

    //qDebug() << Q_FUNC_INFO << data;

    bool ok;
    QVariantMap result = QtJson::parse(data, ok).toMap();

    if(ok) {
        bool success = result["success"].toBool();
        if (!success) {
            qDebug() << Q_FUNC_INFO << ", error: " << result["error"];
            emit failed(result["error"].toString());
            return;
        }

        QVariantMap returned = result["return"].toMap();
        QVariantMap returnedFunds = returned["funds"].toMap();

        Funds funds = fundsFromJson(returnedFunds);
        emit done(funds);
    }
}

RequestArgs SellRequest::constructArgs(QString tickerName, double amount, double rate)
{
    QString pair = tickerName.replace('/', "_").toLower();

    return RequestArgs() << qMakePair(QString("pair"), pair)
                         << qMakePair(QString("type"), QString("sell"))
                         << qMakePair(QString("amount"), QString::number(amount))
                         << qMakePair(QString("rate"), QString::number(rate));
}
