#ifndef BUYREQUEST_H
#define BUYREQUEST_H

#include "abstractrequest.h"
#include "core/common.h"
#include "core/order.h"

class BuyRequest : public AbstractRequest
{
    Q_OBJECT

public:
    explicit BuyRequest(QString tickerName, double amount, double rate,
                         const QString& controllerUrl, NetworkManager* networkManager);
signals:
    void done(Funds);

protected slots:
    virtual void readyRead();

protected:
    RequestArgs constructArgs(QString tickerName, double amount, double rate);
};

#endif // BUYREQUEST_H
