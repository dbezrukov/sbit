#ifndef TRANSACTHISTORYREQUEST_H
#define TRANSACTHISTORYREQUEST_H

#include "abstractrequest.h"
#include "core/common.h"
#include "core/ticker.h"

// Operations with account: refund, withdraw, buy/sell
class TransactHistoryRequest : public AbstractRequest
{
    Q_OBJECT

public:
    explicit TransactHistoryRequest(const QString& controllerUrl, NetworkManager* networkManager);

signals:
    void done(Ticker*);

protected slots:
    virtual void readyRead();
};


#endif // TRANSACTHISTORYREQUEST_H
