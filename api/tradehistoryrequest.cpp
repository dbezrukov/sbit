#include "tradehistoryrequest.h"
#include "qt-json/json.h"

#include <QDebug>

TradeHistoryRequest::TradeHistoryRequest(const QString& controllerUrl, NetworkManager* networkManager)
    : AbstractRequest(controllerUrl, "TradeHistory", RequestArgs(), networkManager)
{
}

void TradeHistoryRequest::readyRead()
{
    QByteArray data = m_reply->readAll();

    //qDebug() << Q_FUNC_INFO << data;

    bool ok;
    QVariantMap result = QtJson::parse(data, ok).toMap();

    if(ok) {
        bool success = result["success"].toBool();
        if (!success) {
            qDebug() << Q_FUNC_INFO << ", error: " << result["error"];
            emit failed(result["error"].toString());
            return;
        }

        QList<Order*> orders;

        QVariantMap ordersInfo = result["return"].toMap();
        //qDebug() << ordersInfo;

        QVariantMap::Iterator it = ordersInfo.begin();

        for (; it != ordersInfo.end(); ++it) {
            qlonglong id = it.key().toLongLong();

            QVariantMap orderInfo = it.value().toMap();

            QString type = orderInfo["type"].toString();
            qlonglong timestampCreated = orderInfo["timestamp_created"].toLongLong();
            QString pair = orderInfo["pair"].toString();
            double rate = orderInfo["rate"].toDouble();
            double amount = orderInfo["amount"].toDouble();

            Order* order = new Order(id, timestampCreated);
            order->setSell(type == "sell" ? true : false);
            order->setTickerName(pair);
            order->setRate(rate);
            order->setamount(amount);
            order->setStatus(1);

            orders << order;
        }

        emit done(orders);
    }
}
