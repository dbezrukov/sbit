#-------------------------------------------------
#
# Project created by QtCreator 2013-05-05T10:50:44
#
#-------------------------------------------------

QT       += core gui network svg webkit

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sbit
TEMPLATE = app

INCLUDEPATH += ../sbit-libs/include

DEBUG_SUFFIX=""
CONFIG(debug, debug|release) {
   LIBS += -L../sbit-libs/lib/debug
   !unix: DEBUG_SUFFIX = d
} else {
   LIBS += -L../sbit-libs/lib/release
}

LIBS += -llibeay32 -lssleay32 -lchartdir51
#-lkdchart$$DEBUG_SUFFIX$$2 lkdchart$$DEBUG_SUFFIX$$2

SOURCES += main.cpp\
    ui/sbitmainwindow.cpp \
    qt-json/json.cpp \
    api/sbitapi.cpp \
    ui/tabmain.cpp \
    ui/tabchart.cpp \
    api/accountinforequest.cpp \
    api/abstractrequest.cpp \
    core/ticker.cpp \
    core/tickersmodel.cpp \
    api/tickerinforequest.cpp \
    ui/tickerdelegate.cpp \
    ui/tickeritemwidget.cpp \
    ui/tickerdelegatecharts.cpp \
    ui/tickeritemwidgetcharts.cpp \
    ui/tickerchartwidget.cpp \
    api/tradehistoryrequest.cpp \
    ui/loginwindow.cpp \
    api/transacthistoryrequest.cpp \
    api/ordersrequest.cpp \
    core/networkmanager.cpp \
    api/sellrequest.cpp \
    api/buyrequest.cpp \
    ui/currencybalancewidget.cpp \
    core/order.cpp \
    core/ordersmodel.cpp \
    ui/orderdelegate.cpp \
    ui/orderitemwidget.cpp \
    ui/ordersheaderwidget.cpp \
    ui/createorderwidget.cpp \
    api/cancelorderrequest.cpp \
    core/common.cpp \
    ui/tabhistory.cpp \
    ui/openbookitemdelegate.cpp \
    core/depthoffer.cpp \
    core/trade.cpp \
    api/depthinforequest.cpp \
    core/depthmodel.cpp \
    core/depthmodeldetails.cpp \
    ui/depthitemwidgets.cpp \
    core/depthfiltermodel.cpp \
    ui/openbookwidget.cpp \
    api/tradeinforequest.cpp \
    core/ohlcdata.cpp \
    core/chartsmodel.cpp \
    api/chartdatarequest.cpp \
    ui/chartview.cpp \
    ui/taborders.cpp \
    ui/controlbarchartwidget.cpp \
    core/ordersmodeldetails.cpp \
    ui/orderitemwidgets.cpp

HEADERS  += ui/sbitmainwindow.h \
    api/sbitapi.h \
    core/common.h \
    ui/tabmain.h \
    ui/tabchart.h \
    api/accountinforequest.h \
    api/abstractrequest.h \
    core/ticker.h \
    core/tickersmodel.h \
    api/tickerinforequest.h \
    ui/tickerdelegate.h \
    ui/tickeritemwidget.h \
    ui/tickerdelegatecharts.h \
    ui/tickeritemwidgetcharts.h \
    ui/tickerchartwidget.h \
    api/tradehistoryrequest.h \
    ui/loginwindow.h \
    api/transacthistoryrequest.h \
    api/ordersrequest.h \
    core/networkmanager.h \
    api/sellrequest.h \
    api/buyrequest.h \
    ui/currencybalancewidget.h \
    core/order.h \
    core/ordersmodel.h \
    ui/orderdelegate.h \
    ui/orderitemwidget.h \
    ui/ordersheaderwidget.h \
    ui/createorderwidget.h \
    api/cancelorderrequest.h \
    ui/tabhistory.h \
    api/tradeinforequest.h \
    ui/openbookitemdelegate.h \
    core/depthoffer.h \
    core/trade.h \
    ui/openbookwidget.h \
    core/ohlcdata.h \
    core/chartsmodel.h \
    api/chartdatarequest.h \
    ui/chartview.h \
    ui/taborders.h \
    api/depthinforequest.h \
    core/depthmodel.h \
    core/depthmodeldetails.h \
    ui/depthitemwidgets.h \
    core/depthfiltermodel.h \
    ui/controlbarchartwidget.h \
    core/ordersmodeldetails.h \
    ui/orderitemwidgets.h

FORMS    += ui/sbitmainwindow.ui \
    ui/tabmain.ui \
    ui/tabchart.ui \
    ui/tickerchartwidget.ui \
    ui/loginwindow.ui \
    ui/ordersheaderwidget.ui \
    ui/orderitemwidget.ui \
    ui/createorderwidget.ui \
    ui/tabhistory.ui \
    ui/taborders.ui \
    ui/controlbarchartwidget.ui \
    ui/taborders.ui

RESOURCES += \
    res/resources.qrc
