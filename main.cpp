#include "ui/sbitmainwindow.h"
#include "ui/loginwindow.h"

#include <QApplication>
#include <QDebug>
#include <QFont>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFont curFont = QApplication::font();
    curFont.setFamily("Segoe UI");
    curFont.setStyleStrategy(QFont::PreferAntialias);

    if (curFont.family() == "Segoe UI")
        QApplication::setFont(curFont);

    SBitMainWindow w;

    QString defaultKey = "S8VMVLJW-Y3Q2NDBO-487XGU0G-J3K9R7CW-NIHSKY5B";
    QString defaultSecret = "8aa75f0422e5f18ad7cbfa12adb626dcdf0b8e2bc3bd7999018f495bfb8834d8";

    LoginWindow loginWindow(defaultKey, defaultSecret);

    QObject::connect(&loginWindow, SIGNAL(cancel()), &a, SLOT(closeAllWindows()));
    QObject::connect(&loginWindow, SIGNAL(login(QString,QString)), &w, SLOT(showWindow(QString, QString)));

    loginWindow.setWindowFlags( Qt::Dialog );
    loginWindow.show();

    return a.exec();
}
