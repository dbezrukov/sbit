#ifndef TICKERITEMWIDGET_H
#define TICKERITEMWIDGET_H

#include <QWidget>
#include <QLabel>

class TickerItemWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TickerItemWidget(QWidget *parent = 0);
    void setName(QString);
    void setPrice(double);
    QSize sizeHint() const;
    
protected:
    QLabel* m_name;
    QLabel* m_price;
};

#endif // TICKERITEMWIDGET_H
