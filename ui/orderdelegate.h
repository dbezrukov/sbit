#ifndef ORDERDELEGATE_H
#define ORDERDELEGATE_H

#include <QItemDelegate>
#include <QPainter>

//#include "orderitemwidget.h"
#include "core/order.h"

class OrderDelegate : public QItemDelegate
{
public:
    OrderDelegate();
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

//protected:
//    OrderItemWidget* m_itemWidget;
};

#endif // ORDERDELEGATE_H
