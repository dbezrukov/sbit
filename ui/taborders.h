#ifndef TABORDERS_H
#define TABORDERS_H

#include <QWidget>
#include <QModelIndex>

namespace Ui {
class TabOrders;
}

class TabOrders : public QWidget
{
    Q_OBJECT
    
public:
    explicit TabOrders(QWidget *parent = 0);
    ~TabOrders();
signals:
    void orderChanged();

protected slots:
    void currentOrderChanged(QModelIndex,QModelIndex);
    
private:
    Ui::TabOrders *ui;
};

#endif // TABORDERS_H
