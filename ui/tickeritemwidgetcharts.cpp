#include "tickeritemwidgetcharts.h"

#include <QHBoxLayout>

TickerItemWidgetCharts::TickerItemWidgetCharts(QWidget *parent)
    : QWidget(parent)
{
    m_name = new QLabel;
    m_name->setObjectName("tickerName");
    m_name->setFixedHeight(20);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins(4,4,4,4));
    layout->addWidget(m_name);

    QFont font;
    font.setBold(true);
    m_name->setFont(font);
}

void TickerItemWidgetCharts::setName(QString name)
{
    m_name->setText(name);
}

void TickerItemWidgetCharts::setSelected(bool selected)
{
    if (selected) {
        m_name->setStyleSheet( "#tickerName{"
                                      "border: 1px solid #8f8f91;"
                                      "color: black;"
                                      "qproperty-alignment: AlignCenter;"
                                      "border-radius: 6px;"
                                      "background-color: rgba(255, 180, 55);"
                                      "min-width: 80px;}");
    } else
    {
        m_name->setStyleSheet( "#tickerName{"
                                      "border: 1px solid #8f8f91;"
                                      "color: black;"
                                      "qproperty-alignment: AlignCenter;"
                                      "border-radius: 6px;"
                                      "background-color: transparent;"
                                      "min-width: 80px;}" );
    }
}

QSize TickerItemWidgetCharts::sizeHint() const
{
    int left, top, right, bottom;
    layout()->getContentsMargins(&left, &top, &right, &bottom);
    return QSize(100, m_name->height() + top + bottom);
}

