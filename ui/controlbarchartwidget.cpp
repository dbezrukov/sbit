#include "controlbarchartwidget.h"
#include "ui_controlbarchartwidget.h"

ControlbarChartWidget::ControlbarChartWidget(int index, const QString& name,int x, int y, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ControlbarChartWidget)
{
    m_index = index;

    ui->setupUi(this);

    ui->labelName->setText(name);
    this->move(x, y);
    this->setVisible(true);

    connect(ui->toolButtonSettings, SIGNAL(clicked()), SLOT(onSettings()));
    connect(ui->toolButtonClose, SIGNAL(clicked()), SLOT(onClose()));
}

ControlbarChartWidget::~ControlbarChartWidget()
{
    delete ui;
}


void ControlbarChartWidget::onSettings()
{
    emit settings(m_index);
}

void ControlbarChartWidget::onClose()
{
    emit close(m_index);
}
