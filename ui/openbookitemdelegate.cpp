#include <QPainter>

#include "openbookitemdelegate.h"
#include "core/depthmodeldetails.h"

OpenBookItemDelegate::OpenBookItemDelegate(QObject *parent) :
    QItemDelegate(parent)
{
}

QSize OpenBookItemDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);

    QSize zeroSize(0, 0);

    if (!index.isValid())
        return zeroSize;
    if (index.column() >= DepthModelDetails::instance().detailsList().size())
        return zeroSize;

    return DepthModelDetails::instance().detailsList()[index.column()]->depthItemWidget().sizeHint();

}

void OpenBookItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (!index.isValid())
        return;
    if (index.column() >= DepthModelDetails::instance().detailsList().size())
        return;

    DepthOffer* item = index.data().value<DepthOffer*>();

    DepthModelDetails::instance().detailsList()[index.column()]->depthItemWidget().initWithDepth(item);

    QPalette pal;

    if (option.state & QStyle::State_Selected) {
        pal.setBrush(QPalette::Window, QBrush(QColor(255, 180, 55)));
    } else {
       pal.setBrush(QPalette::Window, QBrush(Qt::white));
    }
    DepthModelDetails::instance().detailsList()[index.column()]->depthItemWidget().setPalette(pal);
    DepthModelDetails::instance().detailsList()[index.column()]->depthItemWidget().resize(option.rect.size());

    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->translate(option.rect.topLeft());
    DepthModelDetails::instance().detailsList()[index.column()]->depthItemWidget().render(painter);

    painter->restore();
}
