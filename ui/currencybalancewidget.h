#ifndef CURRENCYBALANCEWIDGET_H
#define CURRENCYBALANCEWIDGET_H

#include "core/common.h"
#include <QComboBox>

class CurrencyBalanceWidget : public QComboBox
{
    Q_OBJECT
public:
    explicit CurrencyBalanceWidget(QWidget *parent = 0);
    void setCurrency(QString);

signals:
    
protected slots:
    void accountInfoUpdated();
    void currentIndexChanged(int index);

protected:
    QIcon icon(QString currency);

protected:
    int m_currentIndex;
};

#endif // CURRENCYBALANCEWIDGET_H
