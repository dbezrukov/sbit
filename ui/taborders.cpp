#include "taborders.h"
#include "ui_taborders.h"

#include <QTableView>
#include <QHeaderView>

#include "core/common.h"
#include "core/order.h"
#include "api/sbitapi.h"
#include "ui/orderdelegate.h"

TabOrders::TabOrders(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TabOrders)
{
    ui->setupUi(this);

    QVBoxLayout* pVBoxLayout = new QVBoxLayout(ui->widget);
    pVBoxLayout->setContentsMargins(QMargins());
    QTableView* pTableView = new QTableView(this);
    pVBoxLayout->addWidget(pTableView);

    pTableView->setStyleSheet("QHeaderView::section {"
                              "  color: rgba(136, 144, 152, 255);"
                              "  font: bold 14px;"
                              "  }");

    pTableView->setModel(SBitApi::instance()->orders());
    pTableView->setItemDelegate(new OrderDelegate());
    pTableView->verticalHeader()->hide();
    pTableView->horizontalHeader()->setHighlightSections(false);
    pTableView->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    pTableView->setSelectionMode(QAbstractItemView::SingleSelection);
    pTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    pTableView->setSelectionModel(SBitApi::instance()->orders()->selectionModel());

    // fixme, this connectin doesn't work atm
    connect(pTableView->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
        this, SLOT(currentOrderChanged(QModelIndex,QModelIndex)));
}

TabOrders::~TabOrders()
{
    delete ui;
}

void TabOrders::currentOrderChanged(QModelIndex current, QModelIndex)
{
    if (current.isValid()) {
        if (Order* item = current.data().value<Order*>()) {
            //ui->orderChange->setEnabled(true);
            //ui->orderClose->setEnabled(true);
            //return;
            emit orderChanged();
        }
    }
    //ui->orderChange->setEnabled(false);
    //ui->orderClose->setEnabled(false);
}
