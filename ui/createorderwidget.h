#ifndef CREATEORDERWIDGET_H
#define CREATEORDERWIDGET_H

#include <QWidget>

namespace Ui {
class CreateOrderWidget;
}

class CreateOrderWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit CreateOrderWidget(QWidget *parent = 0, QString defaultPair = QString());
    ~CreateOrderWidget();
    
private slots:
    void on_pairCombo_currentIndexChanged(const QString &arg1);
    void on_typeCombo_currentIndexChanged(int index);
    void on_cancel_clicked();
    void on_done_clicked();
    void on_priceEdit_textChanged(const QString &arg1);
    void on_amountEdit_textChanged(const QString &arg1);

protected:
    void updateTotalCost();

private:
    Ui::CreateOrderWidget *ui;
};

#endif // CREATEORDERWIDGET_H
