#ifndef SBITMAINWINDOW_H
#define SBITMAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class SBitMainWindow;
}

class SBitMainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit SBitMainWindow(QWidget *parent = 0);
    ~SBitMainWindow();

protected:
    QWidget* createTabMain();
    QWidget* createTabOrders();
    QWidget* createTabChart();
    QWidget* createTabHistory();

public slots:
    void showWindow(QString, QString);

protected slots:
    void actionAccountConfig();
    void actionSignout();
    void actionExit();

private:
    Ui::SBitMainWindow *ui;
};

#endif // SBITMAINWINDOW_H
