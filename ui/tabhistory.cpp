#include "tabhistory.h"
#include "ui_tabhistory.h"
#include "core/common.h"
#include "api/sbitapi.h"
#include "ui/orderdelegate.h"

TabHistory::TabHistory(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::TabHistory)
{
    ui->setupUi(this);

    ui->pairsCombo->addItem(trUtf8("Все"));
    foreach (QString pair, TickerNames::list) {
        ui->pairsCombo->addItem(pair);
    }
    ui->pairsCombo->setCurrentIndex(0);

    ui->ordersHistory->setStyleSheet("QHeaderView::section {"
                              "  color: rgba(136, 144, 152, 255);"
                              "  font: bold 14px;"
                              "  }");

    ui->ordersHistory->setModel(SBitApi::instance()->ordersHistory());
    ui->ordersHistory->setItemDelegate(new OrderDelegate());
    ui->ordersHistory->verticalHeader()->hide();
    ui->ordersHistory->horizontalHeader()->setHighlightSections(false);
    ui->ordersHistory->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    ui->ordersHistory->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->ordersHistory->setSelectionBehavior(QAbstractItemView::SelectRows);
    //ui->ordersHistory->setSelectionModel(SBitApi::instance()->orders()->selectionModel());
}

TabHistory::~TabHistory()
{
    delete ui;
}
