#ifndef TABMAIN_H
#define TABMAIN_H

#include <QWidget>
#include <QDockWidget>
#include <QModelIndex>

namespace Ui {
class TabMain;
}

class TabMain : public QWidget
{
    Q_OBJECT
    
public:
    explicit TabMain(QWidget *parent = 0);
    ~TabMain();

protected:
    void setupTableHeaders();
    QDockWidget* createDock(QWidget*, QString);

protected slots:
    void accountInfoUpdated();
    void tickersInfoUpdated();
    void currentOrderChanged(QModelIndex,QModelIndex);
    void tickerDoubleClicked(QModelIndex);
    void tickerClicked(QModelIndex);
    void floatingChanged(bool);
    void currentTickerChanged(QModelIndex,QModelIndex);

private slots:
    void on_orderNew_clicked();

    void on_orderClose_clicked();

    void on_orderChange_clicked();

    void on_orderChanged();

    void on_domButton_clicked();

    void on_graphicButton_clicked();

private:
    Ui::TabMain *ui;
};

#endif // TABMAIN_H
