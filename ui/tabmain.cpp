#include "tabmain.h"
#include "ui_tabmain.h"
#include "core/common.h"
#include "core/order.h"
#include "core/ticker.h"
#include "api/sbitapi.h"
#include "ui/tickerdelegate.h"
#include "ui/orderdelegate.h"
#include "ui/createorderwidget.h"
#include "ui/openbookwidget.h"
#include "ui/tickerchartwidget.h"

#include <QMessageBox>

TabMain::TabMain(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::TabMain)
{
    ui->setupUi(this);
    setupTableHeaders();

    connect(SBitApi::instance(), SIGNAL(accountInfoUpdated()), this, SLOT(accountInfoUpdated()));
    connect(SBitApi::instance(), SIGNAL(tickersInfoUpdated()), this, SLOT(tickersInfoUpdated()));

    accountInfoUpdated();

    ui->pricesList->setModel(SBitApi::instance()->tickers());
    ui->pricesList->setSelectionModel(SBitApi::instance()->tickers()->selectionModel());
    ui->pricesList->setItemDelegate(new TickerDelegate);
    connect(ui->pricesList, SIGNAL(doubleClicked(QModelIndex)),
            this, SLOT(tickerDoubleClicked(QModelIndex)));
    connect(ui->pricesList,SIGNAL(clicked(QModelIndex)),
            this, SLOT(tickerClicked(QModelIndex)));

    ui->balanceCurrency1->setCurrency(Currency::clr);
    ui->balanceCurrency2->setCurrency(Currency::btc);
    ui->balanceCurrency3->setCurrency(Currency::usd);

    ui->orderNew->setEnabled(false);
    ui->orderChange->setEnabled(false);
    ui->orderClose->setEnabled(false);

    connect(ui->pricesList->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this, SLOT(currentTickerChanged(QModelIndex,QModelIndex)));
}

TabMain::~TabMain()
{
    delete ui;
}

void TabMain::currentTickerChanged(QModelIndex current, QModelIndex)
{
    if(Ticker* item = current.data().value<Ticker*>()) {
        ui->domButton->setEnabled(true);
        ui->graphicButton->setEnabled(true);

        return;
    }

    ui->domButton->setEnabled(false);
    ui->graphicButton->setEnabled(false);
}

void TabMain::setupTableHeaders()
{

}

void TabMain::accountInfoUpdated()
{
    //double usd = SBitApi::instance()->funds()[RUR];
    //ui->balance->setText(QString::number(usd));
}

void TabMain::tickersInfoUpdated()
{
    ui->orderNew->setEnabled(true);
}

void TabMain::currentOrderChanged(QModelIndex current, QModelIndex)
{
    if (current.isValid()) {
        if (Order* item = current.data().value<Order*>()) {
            ui->orderChange->setEnabled(true);
            ui->orderClose->setEnabled(true);
            return;
        }
    }
    ui->orderChange->setEnabled(false);
    ui->orderClose->setEnabled(false);
}

void TabMain::tickerDoubleClicked(QModelIndex)
{
    on_orderNew_clicked();
}

void TabMain::tickerClicked(QModelIndex)
{
    // test code
    // return;
    QModelIndex index = ui->pricesList->currentIndex();

    if (index.isValid()) {
        Ticker* item = index.data().value<Ticker*>();
        if (item) {
            /*
            TradesModel* pTradeModel = SBitApi::instance()->tradeModel(item->name());
            Trade* pTestTrade = new Trade(pTradeModel->rowCount(), 0, this);
            pTestTrade->setPrice(15);
            pTestTrade->setAmount(2);
            pTestTrade->setSell(true);
            pTradeModel->append(pTestTrade);

            Trade* pTestTrade2 = new Trade(pTradeModel->rowCount(), 0, this);
            pTestTrade2->setPrice(150);
            pTestTrade2->setAmount(13);
            pTestTrade2->setSell(false);
            pTradeModel->append(pTestTrade2);
            */
        }
    }
}

void TabMain::on_orderNew_clicked()
{
    QModelIndex index = ui->pricesList->currentIndex();

    if (index.isValid()) {
        Ticker* item = index.data().value<Ticker*>();
        if (item) {
            CreateOrderWidget* createOrder = new CreateOrderWidget(this, item->name());

            createOrder->setWindowFlags( Qt::Dialog );
            createOrder->show();
        }
    }
}

void TabMain::on_orderClose_clicked()
{
    /*
    QModelIndex index = ui->ordersList->currentIndex();
    if (index.isValid()) {
        if (Order* item = index.data().value<Order*>()) {

            QMessageBox msgBox;
            msgBox.setWindowTitle("UpBit terminal");
            msgBox.setText(trUtf8("Закрыть сделку?"));
            msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
            msgBox.setDefaultButton(QMessageBox::Ok);
            int ret = msgBox.exec();
            if (ret == QMessageBox::Ok) {
                SBitApi::instance()->cancelOrder(QString::number(item->id()));
            }
        }
    }
    */
}

void TabMain::on_orderChange_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("UpBit terminal");
    msgBox.setText(trUtf8("Функция в разработке"));
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();
}

void TabMain::on_orderChanged()
{
    ui->orderChange->setEnabled(true);
    ui->orderClose->setEnabled(true);
}

QDockWidget* TabMain::createDock(QWidget* widget, QString title)
{
    QDockWidget* dockWidget = new QDockWidget(title);
    QWidget* view = new QWidget();

    dockWidget->setWidget(view);
    dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea |
                                Qt::RightDockWidgetArea |
                                Qt::BottomDockWidgetArea |
                                Qt::TopDockWidgetArea);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->setMargin(0);
    layout->setSpacing(0);

    view->setLayout(layout);

    layout->addWidget(widget);

    connect(dockWidget, SIGNAL(topLevelChanged(bool)), this, SLOT(floatingChanged(bool)));

    return dockWidget;
}

void TabMain::floatingChanged(bool floating)
{
    if(floating)
    {
        QDockWidget* dw = qobject_cast<QDockWidget*>(QObject::sender());

        dw->setWindowFlags(Qt::Window);
        dw->show();
    }
}

void TabMain::on_domButton_clicked()
{
    QModelIndex index = ui->pricesList->currentIndex();

    if (index.isValid()) {
        Ticker* item = index.data().value<Ticker*>();
        if (item) {
            //OpenBookWidget* pOpenBook = new OpenBookWidget(item->name(), this);
            //addDockWidget(Qt::RightDockWidgetArea, pOpenBook);
            //pOpenBook->show();

            ui->gridLayout->addWidget(createDock(new OpenBookWidget(item->name(), this),item->name()));
        }
    }
}

void TabMain::on_graphicButton_clicked()
{
    QModelIndex index = ui->pricesList->currentIndex();

    if (index.isValid()) {
        Ticker* item = index.data().value<Ticker*>();
        if (item) {
            ui->gridLayout->addWidget(createDock(new TickerChartWidget(item, this), item->name()));
        }
    }
}
