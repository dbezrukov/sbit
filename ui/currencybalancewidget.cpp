#include "currencybalancewidget.h"
#include "api/sbitapi.h"

CurrencyBalanceWidget::CurrencyBalanceWidget(QWidget *parent)
    : QComboBox(parent)
    , m_currentIndex(0)
{
    foreach (QString currency, Currency::list) {
        addItem(icon(currency), currency);
    }

    setCurrentIndex(0);

    connect(SBitApi::instance(), SIGNAL(accountInfoUpdated()), this, SLOT(accountInfoUpdated()));
    connect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexChanged(int)));

    accountInfoUpdated();
}

void CurrencyBalanceWidget::setCurrency(QString currency)
{
    setCurrentIndex(Currency::list.indexOf(currency));
}

void CurrencyBalanceWidget::accountInfoUpdated()
{
    double value = SBitApi::instance()->funds()[Currency::list.at(m_currentIndex)];
    CurrencyBalanceWidget::setItemText(m_currentIndex, QString::number(value, 'g', 4));
}

void CurrencyBalanceWidget::currentIndexChanged(int index)
{
    m_currentIndex = index;
    accountInfoUpdated();
}

QIcon CurrencyBalanceWidget::icon(QString currency)
{
    if (currency == Currency::clr)
        return QIcon(":/currency/lark24.png");
    else if (currency == Currency::btc)
        return QIcon(":/currency/bitcoin24.png");
    else if (currency == Currency::usd)
        return QIcon(":/currency/usd24.png");
    else if (currency == Currency::eur)
        return QIcon(":/currency/euro24.png");
    else if (currency == Currency::ltc)
        return QIcon(":/currency/ltc24.png");
    else if (currency == Currency::rur)
        return QIcon(":/currency/rur24.png");
    else
        return QIcon();
}
