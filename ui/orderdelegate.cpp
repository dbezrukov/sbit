#include "orderdelegate.h"

#include <QDebug>
#include "core/ordersmodeldetails.h"

OrderDelegate::OrderDelegate()
{
    //m_itemWidget = new OrderItemWidget();
}

void OrderDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    /*
    Order* item = index.data().value<Order*>();

    m_itemWidget->setOrder(item);

    QPalette pal;

    if ((option.state & QStyle::State_Selected) == QStyle::State_Selected) {
        pal.setBrush(QPalette::Window, QBrush(QColor(255, 180, 55)));
    }
    else {
        //if (index.row() % 2 == 0) {
            pal.setBrush(QPalette::Window, QBrush(Qt::white));
        //} else {
        //    pal.setBrush(QPalette::Window, QBrush(QColor(227, 232, 237)));
        //}
    }

    m_itemWidget->setPalette(pal);
    m_itemWidget->resize(option.rect.size());

    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->translate(option.rect.topLeft());
    m_itemWidget->render(painter);

    painter->restore();
    */

    if (!index.isValid())
        return;
    if (index.column() >= OrdersModelDetails::instance().detailsList().size())
        return;

    Order* item = index.data().value<Order*>();

    OrdersModelDetails::instance().detailsList()[index.column()]->orderItemWidget().initWithOrder(item);

    QPalette pal;

    if (option.state & QStyle::State_Selected) {
        pal.setBrush(QPalette::Window, QBrush(QColor(255, 180, 55)));
    } else {
       pal.setBrush(QPalette::Window, QBrush(Qt::white));
    }
    OrdersModelDetails::instance().detailsList()[index.column()]->orderItemWidget().setPalette(pal);
    OrdersModelDetails::instance().detailsList()[index.column()]->orderItemWidget().resize(option.rect.size());

    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->translate(option.rect.topLeft());
    OrdersModelDetails::instance().detailsList()[index.column()]->orderItemWidget().render(painter);

    painter->restore();
}

QSize OrderDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);

    QSize zeroSize(0, 0);

    if (!index.isValid())
        return zeroSize;
    if (index.column() >= OrdersModelDetails::instance().detailsList().size())
        return zeroSize;

    return OrdersModelDetails::instance().detailsList()[index.column()]->orderItemWidget().sizeHint();

}
