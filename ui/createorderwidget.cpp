#include "createorderwidget.h"
#include "ui_createorderwidget.h"

#include <QDebug>

#include "core/common.h"
#include "api/sbitapi.h"

CreateOrderWidget::CreateOrderWidget(QWidget *parent, QString defaultPair)
    : QWidget(parent),
    ui(new Ui::CreateOrderWidget)
{
    ui->setupUi(this);

    foreach (QString pair, TickerNames::list) {
        ui->pairCombo->addItem(pair);
    }

    int currentIndex = defaultPair.isEmpty() ? 0 : ui->pairCombo->findText(defaultPair);
    ui->pairCombo->setCurrentIndex(currentIndex);

    ui->typeCombo->addItem(trUtf8("Купить"));
    ui->typeCombo->addItem(trUtf8("Продать"));
    ui->typeCombo->setCurrentIndex(0);

    ui->priceEdit->setValidator(new QDoubleValidator(0.000001, 1000000, 6, ui->priceEdit));
    ui->amountEdit->setValidator(new QDoubleValidator(0.000001, 1000000, 6, ui->amountEdit));
}

CreateOrderWidget::~CreateOrderWidget()
{
    delete ui;
}

void CreateOrderWidget::on_pairCombo_currentIndexChanged(const QString &arg1)
{
    QStringList currency = arg1.split("/");
    ui->labelCurrency->setText(currency[0]);
    ui->labelCurrencyTotal->setText(currency[1]);
    ui->labelCurrency2->setText(currency[1]);

    Ticker* ticker = SBitApi::instance()->tickers()->item(arg1);
    ui->priceEdit->setText(QString::number(ticker->price()));
}

void CreateOrderWidget::on_typeCombo_currentIndexChanged(int index)
{
    bool buy = (index == 0);

    ui->done->setText(buy ? trUtf8("Купить") : trUtf8("Продать"));
}

void CreateOrderWidget::on_cancel_clicked()
{
    close();
}

void CreateOrderWidget::on_done_clicked()
{
    bool buy = (ui->typeCombo->currentIndex() == 0);

    if (buy) {
        SBitApi::instance()->placeBuyOrder(
                        ui->pairCombo->currentText(),
                        ui->amountEdit->text().toDouble(),
                        ui->priceEdit->text().toDouble());
    } else {
        SBitApi::instance()->placeSellOrder(
                        ui->pairCombo->currentText(),
                        ui->amountEdit->text().toDouble(),
                        ui->priceEdit->text().toDouble());    }
    close();
}

void CreateOrderWidget::on_priceEdit_textChanged(const QString&)
{
    updateTotalCost();
}

void CreateOrderWidget::on_amountEdit_textChanged(const QString&)
{
    updateTotalCost();
}

void CreateOrderWidget::updateTotalCost()
{
    ui->costEdit->setText(QString::number(ui->amountEdit->text().toDouble() * ui->priceEdit->text().toDouble()));
}
