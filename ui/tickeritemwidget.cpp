#include "tickeritemwidget.h"

#include <QHBoxLayout>

TickerItemWidget::TickerItemWidget(QWidget *parent)
    : QWidget(parent)
{
    m_name = new QLabel;
    m_price = new QLabel;

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins(10,0,4,0));
    layout->addWidget(m_name);
    layout->addWidget(m_price);

    QFont font;
    font.setBold(true);
    m_name->setFont(font);

    setFixedHeight(25);
}

void TickerItemWidget::setName(QString name)
{
    m_name->setText(name);
}

void TickerItemWidget::setPrice(double price)
{
    if (price > 0.f) {
        m_price->setText(QString::number(price));
    } else {
        m_price->setText("-");
    }
}

QSize TickerItemWidget::sizeHint() const
{
    return QSize(-1, 25);
}
