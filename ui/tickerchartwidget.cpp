#include "tickerchartwidget.h"
#include "ui_tickerchartwidget.h"
#include "api/sbitapi.h"

#include <QWebView>
#include <QDebug>

TickerChartWidget::TickerChartWidget(Ticker* ticker, QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::TickerChartWidget)
    , m_ticker(ticker)
{
    ui->setupUi(this);

    m_chartView = new ChartView(ui->scrollArea);
    m_chartView->setModel(SBitApi::instance()->chartModel(tickerName()));

    ui->scrollArea->setWidget(m_chartView);

//    m_chartViewer->setMouseUsage(Chart::MouseUsageZoomIn);
//    qDebug() << "ticker name = " << tickerName();

    ui->comboBoxBars->insertItem(0, tr("RSI"));
    ui->comboBoxBars->insertItem(1, tr("MACD"));

    connect(ui->toolButtonInsert, SIGNAL(clicked()), SLOT(insertBar()));
    connect(ui->toolButtonDelete, SIGNAL(clicked()), SLOT(deleteBar()));
}

TickerChartWidget::~TickerChartWidget()
{
    delete ui;
}

QString TickerChartWidget::tickerName()
{
    return m_ticker->name();
}

void TickerChartWidget::resizeEvent(QResizeEvent *e)
{
    m_chartView->setSize(e->size().width() - 20, e->size().height() - 80 - 50);
}

void TickerChartWidget::insertBar()
{
    m_chartView->setBar(ui->comboBoxBars->currentIndex(), true);
}

void TickerChartWidget::deleteBar()
{
    m_chartView->setBar(ui->comboBoxBars->currentIndex(), false);
}
