#ifndef TICKERDELEGATECHARTS_H
#define TICKERDELEGATECHARTS_H

#include <QItemDelegate>
#include <QPainter>

#include "tickeritemwidgetcharts.h"
#include "core/ticker.h"

class TickerDelegateCharts : public QItemDelegate
{
public:
    TickerDelegateCharts();
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

protected:
    TickerItemWidgetCharts* m_itemWidget;
};

#endif // TICKERDELEGATECHARTS_H
