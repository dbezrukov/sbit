#ifndef CONTROLBARCHARTWIDGET_H
#define CONTROLBARCHARTWIDGET_H

#include <QWidget>

namespace Ui {
class ControlbarChartWidget;
}

class ControlbarChartWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit ControlbarChartWidget(int index, const QString& name, int x, int y, QWidget *parent = 0);
    ~ControlbarChartWidget();
    
private:
    Ui::ControlbarChartWidget *ui;

signals:
    void settings(int index);
    void close(int index);

private slots:
    void onSettings();
    void onClose();

private:
    int m_index;
};

#endif // CONTROLBARCHARTWIDGET_H
