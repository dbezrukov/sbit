#include "chartview.h"
#include <time.h>
#include <QMouseEvent>
#include <QToolTip>
#include <QCursor>
#include <QBitmap>

#include <vector>
#include <sstream>
#include <algorithm>

#include <QDebug>

#include "ui/controlbarchartwidget.h"

using namespace std;

/////////////////////////////////////////////////////////////////////////////
// ChartView

//
// Build in mouse cursors for zooming and scrolling support
//
static QCursor &getZoomInCursor();
static QCursor &getZoomOutCursor();
static QCursor &getNoZoomCursor();
static QCursor &getNoMove2DCursor();
static QCursor &getNoMoveHorizCursor();
static QCursor &getNoMoveVertCursor();

//
// Constants used in m_delayChartUpdate
//
enum { NO_DELAY, NEED_DELAY, NEED_UPDATE };

ChartView::ChartView(QWidget *parent) :
    QLabel(parent)
{
    // current chart and hot spot tester
    m_currentChart = 0;
    m_hotSpotTester = 0;

    // initialize chart configuration
    m_selectBoxLineColor = QColor(0, 0, 0);
    m_selectBoxLineWidth = 2;
    m_mouseUsage = Chart::MouseUsageDefault;
    m_zoomDirection = Chart::DirectionHorizontal;
    m_zoomInRatio = 2;
    m_zoomOutRatio = 0.5;
    m_scrollDirection = Chart::DirectionHorizontal;
    m_minDragAmount = 5;
    m_updateInterval = 20;

    // current state of the mouse
    m_isOnPlotArea = false;
    m_isPlotAreaMouseDown = false;
    m_isDragScrolling = false;
    m_currentHotSpot = -1;
    m_isClickable = false;
    m_isMouseTracking = false;
    m_isInMouseMove = false;

    // chart update rate support
    m_needUpdateChart = false;
    m_needUpdateImageMap = false;
    m_holdTimerActive = false;
    m_isInViewPortChanged = false;
    m_delayUpdateChart = NO_DELAY;
    m_delayedChart = 0;
    m_lastMouseMove = 0;
    m_delayedMouseEvent = 0;

    // track cursor support
    m_autoHideMsg = "";
    m_currentMouseX = 0;
    m_currentMouseY = 0;
    m_isInMouseMovePlotArea = false;

    // selection rectangle
    m_LeftLine = 0;
    m_RightLine = 0;
    m_TopLine = 0;
    m_BottomLine = 0;

    setMouseTracking(true);

    m_financeChart = NULL;
    m_model = NULL;

    m_bars[0] = m_bars[1] = false;
    controlBar[0] = controlBar[1] = NULL;

    connect(this, SIGNAL(mouseMovePlotArea(QMouseEvent*)),
        SLOT(onMouseMovePlotArea(QMouseEvent*)));

//    this->drawChart();

    connect(&m_timer, SIGNAL(timeout()), SLOT(onTimeout()));

    m_timer.start(1000);

}

ChartView::~ChartView()
{
    delete m_hotSpotTester;
    delete m_delayedMouseEvent;
}

/////////////////////////////////////////////////////////////////////////////
// ChartView message handlers

//
// MouseMove event handler
//
void ChartView::mouseMoveEvent(QMouseEvent *event)
{
    // Enable mouse tracking to detect mouse leave events
    m_isMouseTracking = true;

    emit mouseMove(event);

    // On Windows, mouse events can by-pass the event queue. If there are too many mouse events,
    // the event queue may not get processed, preventing other controls from updating. If two mouse
    // events are less than 10ms apart, there is a risk of too many mouse events. So we repost the
    // mouse event as a timer event that is queued up normally, allowing the queue to get processed.
    unsigned int timeBetweenMouseMove = (((unsigned int)clock()) - m_lastMouseMove)
                                        * 1000 / CLOCKS_PER_SEC ;
    if ((m_delayedMouseEvent && (timeBetweenMouseMove < 250)) || (timeBetweenMouseMove < 10))
    {
        if (!m_delayedMouseEvent)
            m_delayedMouseEventTimerId = startTimer(1);
        else
            delete m_delayedMouseEvent;

        m_delayedMouseEvent = new QMouseEvent(event->type(), event->pos(), event->globalPos(),
            event->button(), event->buttons(), event->modifiers());
    }
    else
        commitMouseMove(event);

    onSetCursor();
}

//
// The method that actually performs MouseMove event processing
//
void ChartView::commitMouseMove(QMouseEvent *event)
{
    // Remember the mouse coordinates for later use
    m_currentMouseX = event->x();
    m_currentMouseY = event->y();

    // The chart can be updated more than once during mouse move. For example, it can update due to
    // drag to scroll, and also due to drawing track cursor. So we delay updating the display until
    // all all events has occured.
    m_delayUpdateChart = NEED_DELAY;
    m_isInMouseMove = true;

    // Check if mouse is dragging on the plot area
    m_isOnPlotArea = m_isPlotAreaMouseDown || inPlotArea(event->x(), event->y());
    if (m_isPlotAreaMouseDown)
        onPlotAreaMouseDrag(event);

    // Emit mouseMoveChart
    emit mouseMoveChart(event);

    if (inExtendedPlotArea(event->x(), event->y()))
    {
        // Mouse is in extended plot area, emit mouseMovePlotArea
        m_isInMouseMovePlotArea = true;
        emit mouseMovePlotArea(event);
    }
    else if (m_isInMouseMovePlotArea)
    {
        // Mouse was in extended plot area, but is not in it now, so emit mouseLeavePlotArea
        m_isInMouseMovePlotArea = false;
        emit mouseLeavePlotArea(event);
        applyAutoHide("mouseleaveplotarea");
    }

    //
    // Show hot spot tool tips if necessary
    // (Due to QT bug, tooltip cannot be put in delayedMouseEvent, otherwise sometimes tooltip
    // position will be incorrect.)
    //

    if (event->buttons() != Qt::NoButton)
    {
        // Hide tool tips if mouse button is pressed.
        QToolTip::hideText();
    }
    else
    {
        // Use the ChartDirector ImageMapHandler to determine if the mouse is over a hot spot
        int hotSpotNo = 0;
        if (0 != m_hotSpotTester)
            hotSpotNo = m_hotSpotTester->getHotSpot(event->x(), event->y());

        // If the mouse is in the same hot spot since the last mouse move event, there is no need
        // to update the tool tip.
        if (hotSpotNo != m_currentHotSpot)
        {
            // Hot spot has changed - update tool tip text
            m_currentHotSpot = hotSpotNo;

            if (hotSpotNo == 0)
            {
                // Mouse is not actually on hanlder hot spot - use default tool tip text and reset
                // the clickable flag.
                QToolTip::showText(event->globalPos(), m_defaultToolTip);
                m_isClickable = false;
            }
            else
            {
                // Mouse is on a hot spot. In this implementation, we consider the hot spot as
                // clickable if its href ("path") parameter is not empty.
                const char *path = m_hotSpotTester->getValue("path");
                m_isClickable = ((0 != path) && (0 != *path));
                QToolTip::showText(event->globalPos(), QString::fromUtf8(m_hotSpotTester->getValue("title")));
            }
        }
    }

    // Cancel the delayed mouse event if any
    if (m_delayedMouseEvent)
    {
        killTimer(m_delayedMouseEventTimerId);
        delete m_delayedMouseEvent;
        m_delayedMouseEvent = 0;
    }

    // Can update chart now
    commitUpdateChart();
    m_isInMouseMove = false;

    m_lastMouseMove = (unsigned int)clock();
}

//
// Delayed MouseMove event handler
//
void ChartView::onDelayedMouseMove()
{
    if (m_delayedMouseEvent)
        commitMouseMove(m_delayedMouseEvent);
}

void ChartView::leaveEvent(QEvent *event)
{
    // Process delayed mouse move, if any
    onDelayedMouseMove();

    // Mouse tracking is no longer active
    m_isMouseTracking = false;

    if (m_isInMouseMovePlotArea)
    {
        // Mouse was in extended plot area, but is not in it now, so emit mouseLeavePlotArea
        m_isInMouseMovePlotArea = false;
        emit mouseLeavePlotArea(event);
        applyAutoHide("mouseleaveplotarea");
    }

    // emit mouseLeaveChart
    emit mouseLeaveChart(event);
    applyAutoHide("mouseleavechart");
}

void ChartView::wheelEvent(QWheelEvent *event)
{
    if (receivers(SIGNAL(mouseWheel(QWheelEvent *))) <= 0)
        event->ignore();
    else
    {
        // Process delayed mouse move, if any
        onDelayedMouseMove();

        // emit mouseWheel event
        emit mouseWheel(event);
    }
}

//
// Change the mouse cursor.
//
void ChartView::onSetCursor()
{
    if (this->m_isDragScrolling)
    {
        switch (m_scrollDirection)
        {
        case Chart::DirectionHorizontal:
            setCursor(getNoMoveHorizCursor());
            break;
        case Chart::DirectionVertical:
            setCursor(getNoMoveVertCursor());
            break;
        default :
            setCursor(getNoMove2DCursor());
            break;
        }

        return;
    }

    if (m_isOnPlotArea)
    {
        switch (m_mouseUsage)
        {
        case Chart::MouseUsageZoomIn:
            if (canZoomIn(m_zoomDirection))
                setCursor(getZoomInCursor());
            else
                setCursor(getNoZoomCursor());
            return;
        case Chart::MouseUsageZoomOut:
            if (canZoomOut(m_zoomDirection))
                setCursor(getZoomOutCursor());
            else
                setCursor(getNoZoomCursor());
            return;
        }
    }

    if (m_isClickable)
        setCursor(Qt::PointingHandCursor);
    else
        unsetCursor();
}

void ChartView::createControlBar()
{
    int countbar = 0, hadd = 0;

    for (int i = 0; i < 2; i++)
    {
        if (m_bars[i])
        {
            int x, y;

            x = m_width - 110;
            y = m_height - heightBars() + 30 + (countbar*75) + hadd;

            if (!controlBar[i])
            {
                ControlbarChartWidget* control = new ControlbarChartWidget(i, "", x, y, this);
                controlBar[i] = control;

                connect(control, SIGNAL(close(int)), SLOT(onControlBarActive(int)));
            }
            else
            {
                controlBar[i]->move(x, y);
            }

            ++countbar;
            if (countbar) hadd = 3;
        }
    }
}

int ChartView::heightBars()
{
    int height = 0;

    for (int i = 0; i < 2; i++)
    {
        if (m_bars[i])
        {
            height += 75;
        }
    }

    return height;
}

//
// Mouse left button down event.
//

void ChartView::mousePressEvent(QMouseEvent *event)
{
    onDelayedMouseMove();

    if ((event->button() == Qt::LeftButton) && inPlotArea(event->x(), event->y()) &&
        (m_mouseUsage != Chart::MouseUsageDefault))
    {
        // Mouse usage is for drag to zoom/scroll. Capture the mouse to prepare for dragging and
        // save the mouse down position to draw the selection rectangle.
        m_isPlotAreaMouseDown = true;
        m_plotAreaMouseDownXPos = event->x();
        m_plotAreaMouseDownYPos = event->y();
        startDrag();
    }
}

//
// Mouse left button up event.
//
void ChartView::mouseReleaseEvent(QMouseEvent *event)
{
    onDelayedMouseMove();

    if ((event->button() == Qt::LeftButton) && m_isPlotAreaMouseDown)
    {
        // Release the mouse capture.
        m_isPlotAreaMouseDown = false;
        setRectVisible(false);
        bool hasUpdate = false;

        switch (m_mouseUsage)
        {
        case Chart::MouseUsageZoomIn :
            if (canZoomIn(m_zoomDirection))
            {
                if (isDrag(m_zoomDirection, event))
                    // Zoom to the drag selection rectangle.
                    hasUpdate = zoomTo(m_zoomDirection,
                        m_plotAreaMouseDownXPos, m_plotAreaMouseDownYPos, event->x(), event->y());
                else
                    // User just click on a point. Zoom-in around the mouse cursor position.
                    hasUpdate = zoomAt(m_zoomDirection, event->x(), event->y(), m_zoomInRatio);
            }
            break;
        case Chart::MouseUsageZoomOut:
            // Zoom out around the mouse cursor position.
            if (canZoomOut(m_zoomDirection))
                hasUpdate = zoomAt(m_zoomDirection, event->x(), event->y(), m_zoomOutRatio);
            break;
        default :
            if (m_isDragScrolling)
                // Drag to scroll. We can update the image map now as scrolling has finished.
                updateViewPort(false, true);
            else
                // Is not zooming or scrolling, so is just a normal click event.
                emit clicked(event);
            break;
        }

        m_isDragScrolling = false;
        if (hasUpdate)
            // View port has changed - update it.
            updateViewPort(true, true);
    }
    else
        emit clicked(event);

    onSetCursor();
}

//
// Chart hold timer.
//
void ChartView::timerEvent(QTimerEvent *event)
{
    if (m_delayedMouseEvent && (event->timerId() == m_delayedMouseEventTimerId))
    {
        // Is a delayed mouse move event
        onDelayedMouseMove();
    }
    else if (m_holdTimerActive && (event->timerId() == m_holdTimerId))
    {
        killTimer(m_holdTimerId);
        m_holdTimerActive = false;

        // If has pending chart update request, handles them now.
        if (m_needUpdateChart || m_needUpdateImageMap)
            updateViewPort(m_needUpdateChart, m_needUpdateImageMap);
    }
}

//
// Set the chart to the control
//
void ChartView::setChart(BaseChart *c)
{
    m_currentChart = c;
    setImageMap(0);

    if (0 != c)
    {
        commitPendingSyncAxis(c);
        if (m_delayUpdateChart != NO_DELAY)
            c->makeChart();
    }

    updateDisplay();
}

/////////////////////////////////////////////////////////////////////////////
// ChartView properties

//
// Get back the same BaseChart pointer provided by the previous setChart call.
//
BaseChart *ChartView::getChart()
{
    return m_currentChart;
}

//
// Set image map used by the chart
//
void ChartView::setImageMap(const char *imageMap)
{
    //delete the existing ImageMapHandler
    if (0 != m_hotSpotTester)
        delete m_hotSpotTester;
    m_currentHotSpot = -1;
    m_isClickable = false;

    //create a new ImageMapHandler to represent the image map
    if ((0 == imageMap) || (0 == *imageMap))
        m_hotSpotTester = 0;
    else
        m_hotSpotTester = new ImageMapHandler(imageMap);
}

//
// Get the image map handler for the chart
//
ImageMapHandler *ChartView::getImageMapHandler()
{
    return m_hotSpotTester;
}

//
// Set the default tool tip to use
//
void ChartView::setDefaultToolTip(QString text)
{
    m_defaultToolTip = text;
}

//
// Set the border width of the selection box
//
void ChartView::setSelectionBorderWidth(int width)
{
    m_selectBoxLineWidth = width;
}

//
// Get the border with of the selection box.
//
int ChartView::getSelectionBorderWidth()
{
    return m_selectBoxLineWidth;
}

//
// Set the border color of the selection box
//
void ChartView::setSelectionBorderColor(QColor c)
{
    m_selectBoxLineColor = c;
    if (m_TopLine != 0)
    {
        QPalette p(c, c);
        m_TopLine->setPalette(p);
        m_LeftLine->setPalette(p);
        m_BottomLine->setPalette(p);
        m_RightLine->setPalette(p);
    }
}

//
// Get the border color of the selection box.
//
QColor ChartView::getSelectionBorderColor()
{
    return m_selectBoxLineColor;
}

//
// Set the mouse usage mode
//
void ChartView::setMouseUsage(int mouseUsage)
{
    m_mouseUsage = mouseUsage;
}

//
// Get the mouse usage mode
//
int ChartView::getMouseUsage()
{
    return m_mouseUsage;
}

//
// Set the zoom direction
//
void ChartView::setZoomDirection(int direction)
{
    m_zoomDirection = direction;
}

//
// Get the zoom direction
//
int ChartView::getZoomDirection()
{
    return m_zoomDirection;
}

//
// Set the scroll direction
//
void ChartView::setScrollDirection(int direction)
{
    m_scrollDirection = direction;
}

//
// Get the scroll direction
//
int ChartView::getScrollDirection()
{
    return m_scrollDirection;
}

//
// Set the zoom-in ratio for mouse click zoom-in
//
void ChartView::setZoomInRatio(double ratio)
{
    m_zoomInRatio = ratio;
}

//
// Get the zoom-in ratio for mouse click zoom-in
//
double ChartView::getZoomInRatio()
{
    return m_zoomInRatio;
}

//
// Set the zoom-out ratio
//
void ChartView::setZoomOutRatio(double ratio)
{
    m_zoomOutRatio = ratio;
}

//
// Get the zoom-out ratio
//
double ChartView::getZoomOutRatio()
{
    return m_zoomOutRatio;
}

//
// Set the minimum mouse drag before the dragging is considered as real. This is to avoid small
// mouse vibrations triggering a mouse drag.
//
void ChartView::setMinimumDrag(int offset)
{
    m_minDragAmount = offset;
}

//
// Get the minimum mouse drag before the dragging is considered as real.
//
int ChartView::getMinimumDrag()
{
    return m_minDragAmount;
}

//
// Set the minimum interval between ViewPortChanged events. This is to avoid the chart being
// updated too frequently. (Default is 20ms between chart updates.) Multiple update events
// arrived during the interval will be merged into one chart update and executed at the end
// of the interval.
//
void ChartView::setUpdateInterval(int interval)
{
    m_updateInterval = interval;
}

//
// Get the minimum interval between ViewPortChanged events.
//
int ChartView::getUpdateInterval()
{
    return m_updateInterval;
}

//
// Check if there is a pending chart update request.
//
bool ChartView::needUpdateChart()
{
    return m_needUpdateChart;
}

//
// Check if there is a pending image map update request.
//
bool ChartView::needUpdateImageMap()
{
    return m_needUpdateImageMap;
}

//
// Get the current mouse x coordinate when used in a mouse move event handler
//
int ChartView::getChartMouseX()
{
    return m_currentMouseX;
}

//
// Get the current mouse y coordinate when used in a mouse move event handler
//
int ChartView::getChartMouseY()
{
    return m_currentMouseY;
}

//
// Get the current mouse x coordinate bounded to the plot area when used in a mouse event handler
//
int ChartView::getPlotAreaMouseX()
{
    int ret = getChartMouseX();
    if (ret < getPlotAreaLeft())
        ret = getPlotAreaLeft();
    if (ret > getPlotAreaLeft() + getPlotAreaWidth())
        ret = getPlotAreaLeft() + getPlotAreaWidth();
    return ret;
}

//
// Get the current mouse y coordinate bounded to the plot area when used in a mouse event handler
//
int ChartView::getPlotAreaMouseY()
{
    int ret = getChartMouseY();
    if (ret < getPlotAreaTop())
        ret = getPlotAreaTop();
    if (ret > getPlotAreaTop() + getPlotAreaHeight())
        ret = getPlotAreaTop() + getPlotAreaHeight();
    return ret;
}

void ChartView::setModel(ChartsModel *chartModel)
{
    m_model = chartModel;
}

ChartsModel* ChartView::model()
{
    return m_model;
}

void ChartView::setSize(int width, int height)
{
    m_width = width;
    m_height = height;

    drawChart();
}

void ChartView::setBar(int index, bool flag)
{
    if ( (index > -1) && (index < 3) )
    {
        m_bars[index] = flag;

        if (!flag)
        {
            delete controlBar[index];
            controlBar[index] =  NULL;
        }
    }

    drawChart();
}

//
// Check if mouse is on the extended plot area
//
bool ChartView::isMouseOnPlotArea()
{
    if (m_isMouseTracking)
        return inExtendedPlotArea(getChartMouseX(), getChartMouseY());
    else
        return false;
}

//
// Check if is currently processing a mouse move event
//
bool ChartView::isInMouseMoveEvent()
{
    return m_isInMouseMove;
}

//
// Check if is currently processing a view port changed event
//
bool ChartView::isInViewPortChangedEvent()
{
    return m_isInViewPortChanged;
}

/////////////////////////////////////////////////////////////////////////////
// ChartView methods

int xx = 0;

//
// Update the display
//
void ChartView::updateDisplay()
{
    if (m_delayUpdateChart == NO_DELAY)
        commitUpdateChart();
    else
    {
        m_delayUpdateChart = NEED_UPDATE;
        delete m_delayedChart;
        m_delayedChart = (0 != m_currentChart) ? new BaseChart(m_currentChart) : 0;
    }
}

//
// Commit chart to display
//
void ChartView::commitUpdateChart()
{
    if (m_delayUpdateChart == NEED_DELAY)
    {
        // No actual update occur
        m_delayUpdateChart = NO_DELAY;
        return;
    }

    // Get the image and metrics for the chart
    BaseChart *c = (m_delayUpdateChart == NEED_UPDATE) ? m_delayedChart : m_currentChart;
    QPixmap image;
    const char *chartMetrics = 0;

    if (0 != c)
    {
        // Output chart as Device Indpendent Bitmap with file headers
        MemBlock m = c->makeChart(Chart::BMP);
        image.loadFromData((uchar *)m.data, (uint)m.len);
        setFixedSize(c->getWidth(), c->getHeight());

        // Get chart metrics
        chartMetrics = c->getChartMetrics();
    }

    // Set the QPixmap for display
    setPixmap(image);

    // Set the chart metrics and clear the image map
    setChartMetrics(chartMetrics);

    // Any delayed chart has been committed
    m_delayUpdateChart = NO_DELAY;
    delete m_delayedChart;
    m_delayedChart = 0;
}

//
// Set the message used to remove the dynamic layer
//
void ChartView::removeDynamicLayer(const char *msg)
{
    m_autoHideMsg = QString(msg ? msg : "").toLower();
    if (m_autoHideMsg == "now")
        applyAutoHide(msg);
}

//
// Attempt to hide the dynamic layer using the specified message
//
void ChartView::applyAutoHide(const char *msg)
{
    if (m_autoHideMsg == msg)
    {
        if (0 != m_currentChart)
            m_currentChart->removeDynamicLayer();
        m_autoHideMsg = "";
        updateDisplay();
    }
}

//
// Create the edges for the selection rectangle
//
void ChartView::initRect()
{
    m_LeftLine = new QLabel(this);
    m_LeftLine->setAutoFillBackground(true);
    m_RightLine = new QLabel(this);
    m_RightLine->setAutoFillBackground(true);
    m_TopLine = new QLabel(this);
    m_TopLine->setAutoFillBackground(true);
    m_BottomLine = new QLabel(this);
    m_BottomLine->setAutoFillBackground(true);

    setSelectionBorderColor(getSelectionBorderColor());
}

//
// Set selection rectangle position and size
//
void ChartView::drawRect(int x, int y, int width, int height)
{
    // Create the edges of the rectangle if not already created
    if (m_TopLine == 0)
        initRect();

    // width < 0 is interpreted as the rectangle extends to the left or x.
    // height <0 is interpreted as the rectangle extends to above y.
    if (width < 0)
        x -= (width = -width);
    if (height < 0)
        y -= (height = -height);

    // Put the edges along the sides of the rectangle
    m_TopLine->move(x, y);
    m_TopLine->setFixedSize(width, m_selectBoxLineWidth);
    m_LeftLine->move(x, y);
    m_LeftLine->setFixedSize(m_selectBoxLineWidth, height);
    m_BottomLine->move(x, y + height - m_selectBoxLineWidth + 1);
    m_BottomLine->setFixedSize(width, m_selectBoxLineWidth);
    m_RightLine->move(x + width - m_selectBoxLineWidth + 1, y);
    m_RightLine->setFixedSize(m_selectBoxLineWidth, height);
}

//
// Show/hide selection rectangle
//
void ChartView::setRectVisible(bool b)
{
    // Create the edges of the rectangle if not already created
    if (b && (m_TopLine == 0))
        initRect();

    // Show/hide the edges
    if (m_TopLine != 0)
    {
        m_TopLine->setVisible(b);
        m_LeftLine->setVisible(b);
        m_BottomLine->setVisible(b);
        m_RightLine->setVisible(b);
    }
}

//
// Determines if the mouse is dragging.
//
bool ChartView::isDrag(int direction, QMouseEvent *event)
{
    // We only consider the mouse is dragging it is has dragged more than m_minDragAmount. This is
    // to avoid small mouse vibrations triggering a mouse drag.
    int spanX = abs(event->x() - m_plotAreaMouseDownXPos);
    int spanY = abs(event->y() - m_plotAreaMouseDownYPos);
    return ((direction != Chart::DirectionVertical) && (spanX >= m_minDragAmount)) ||
        ((direction != Chart::DirectionHorizontal) && (spanY >= m_minDragAmount));
}

//
// Process mouse dragging over the plot area
//
void ChartView::onPlotAreaMouseDrag(QMouseEvent *event)
{
    switch (m_mouseUsage)
    {
        case Chart::MouseUsageZoomIn :
        {
            //
            // Mouse is used for zoom in. Draw the selection rectangle if necessary.
            //

            bool isDragZoom = canZoomIn(m_zoomDirection) && isDrag(m_zoomDirection, event);
            if (isDragZoom)
            {
                int spanX = m_plotAreaMouseDownXPos - event->x();
                int spanY = m_plotAreaMouseDownYPos - event->y();

                switch (m_zoomDirection)
                {
                case Chart::DirectionHorizontal:
                    drawRect(event->x(), getPlotAreaTop(), spanX, getPlotAreaHeight());
                    break;
                case Chart::DirectionVertical:
                    drawRect(getPlotAreaLeft(), event->y(), getPlotAreaWidth(), spanY);
                    break;
                default:
                    drawRect(event->x(), event->y(), spanX, spanY);
                    break;
                }
            }
            setRectVisible(isDragZoom);
            break;
        }
        case Chart::MouseUsageScroll :
        {
            //
            // Mouse is used for drag scrolling. Scroll and update the view port.
            //

            if (m_isDragScrolling || isDrag(m_scrollDirection, event))
            {
                m_isDragScrolling = true;
                if (dragTo(m_scrollDirection,
                    event->x() - m_plotAreaMouseDownXPos, event->y() - m_plotAreaMouseDownYPos))
                    updateViewPort(true, false);
            }
        }
    }
}

//
// Trigger the ViewPortChanged event
//
void ChartView::updateViewPort(bool needUpdateChart, bool needUpdateImageMap)
{
    // Already updating, no need to update again
    if (m_isInViewPortChanged)
        return;

    // Merge the current update requests with any pending requests.
    m_needUpdateChart = m_needUpdateChart || needUpdateChart;
    m_needUpdateImageMap = needUpdateImageMap;

    // Hold timer has not expired, so do not update chart immediately. Keep the requests pending.
    if (m_holdTimerActive)
        return;

    // The chart can be updated more than once during mouse move. For example, it can update due to
    // drag to scroll, and also due to drawing track cursor. So we delay updating the display until
    // all all updates has occured.
    int hasDelayUpdate = (m_delayUpdateChart != NO_DELAY);
    if (!hasDelayUpdate)
        m_delayUpdateChart = NEED_DELAY;

    // Can trigger the ViewPortChanged event.
    validateViewPort();
    m_isInViewPortChanged = true;
    emit viewPortChanged();
    m_isInViewPortChanged = false;

    // Can update chart now
    if (!hasDelayUpdate)
        commitUpdateChart();

    // Clear any pending updates.
    m_needUpdateChart = false;
    m_needUpdateImageMap = false;

    // Set hold timer to prevent multiple chart updates within a short period.
    if (m_updateInterval > 0)
    {
        m_holdTimerActive = true;
        m_holdTimerId = startTimer(m_updateInterval);
    }
}

void ChartView::drawChart()
{
    // Create a finance chart demo containing 100 days of data
    int noOfDays = 100;

    // To compute moving averages starting from the first day, we need to get extra data points before
    // the first day
    int extraDays = 30;

    // In this exammple, we use a random number generator utility to simulate the data. We set up the
    // random table to create 6 cols x (noOfDays + extraDays) rows, using 9 as the seed.
/*    RanTable rantable(9, 6, noOfDays + extraDays);

    // Set the 1st col to be the timeStamp, starting from Sep 4, 2011, with each row representing one
    // day, and counting week days only (jump over Sat and Sun)
    rantable.setDateCol(0, Chart::chartTime(2011, 9, 4), 86400, true);

    // Set the 2nd, 3rd, 4th and 5th columns to be high, low, open and close data. The open value
    // starts from 100, and the daily change is random from -5 to 5.
    rantable.setHLOCCols(1, 100, -5, 5);

    // Set the 6th column as the vol data from 5 to 25 million
    rantable.setCol(5, 50000000, 250000000);

    // Now we read the data from the table into arrays
    DoubleArray timeStamps = rantable.getCol(0);
    DoubleArray highData = rantable.getCol(1);
    DoubleArray lowData = rantable.getCol(2);
    DoubleArray openData = rantable.getCol(3);
    DoubleArray closeData = rantable.getCol(4);
    DoubleArray volData = rantable.getCol(5);*/

    // Create a FinanceChart object of width 720 pixels

    m_financeChart = new FinanceChart(m_width);

    // Add a title to the chart
//    c->addTitle("Finance Chart Demonstration");

    // Disable default legend box, as we are using dynamic legend
    m_financeChart->setLegendStyle("normal", 8, Chart::Transparent, Chart::Transparent);

    // Set the data into the finance chart object
    if (m_model)
    {
        int count = m_model->size();

//        qDebug() << "count=" << count;
        QListIterator<OhlcData*> *i = m_model->iterator();
        double* timestamp = new double[count];
        double* open = new double[count];
        double* hi = new double[count];
        double* low = new double[count];
        double* close = new double[count];
        double* volume = new double[count];
        int j = 0;

        while (i->hasNext()){
            OhlcData* val = i->next();
            timestamp[j] = val->timestamp();
            open[j] = val->priceOpen();
            hi[j] = val->priceHi();
            low[j] = val->priceLow();
            close[j] = val->priceClose();
            volume[j] = val->volume();
//            qDebug() << timestamp[j] << ", " << open[j] << ", " << hi[j] << ", " << low[j] << ", " << close[j] << ", " << volume[j];
            ++j;
        }

        DoubleArray timeStamps(timestamp, count);
        DoubleArray highData(hi, count);
        DoubleArray lowData(low, count);
        DoubleArray openData(open, count);
        DoubleArray closeData(close, count);
        DoubleArray volData(volume, count);

        m_financeChart->setData(timeStamps, highData, lowData, openData, closeData, volData, extraDays);

        delete timestamp;
        delete open;
        delete close;
        delete hi;
        delete low;
    }
//    m_financeChart->setData(timeStamps, highData, lowData, openData, closeData, volData, extraDays);

    // Add the main chart with 240 pixels in height
    m_financeChart->addMainChart(m_height - this->heightBars());

    // Add a 10 period simple moving average to the main chart, using brown color
//    m_financeChart->addSimpleMovingAvg(10, 0x663300);

    // Add a 20 period simple moving average to the main chart, using purple color
//    m_financeChart->addSimpleMovingAvg(20, 0x9900ff);

    // Add candlestick symbols to the main chart, using green/red for up/down days
    m_financeChart->addCandleStick(0x00ff00, 0xff0000);

    // Add 20 days bollinger band to the main chart, using light blue (9999ff) as the border and
    // semi-transparent blue (c06666ff) as the fill color
//    m_financeChart->addBollingerBand(10, 2, 0x9999ff, 0xc06666ff);

    // Add a 75 pixels volume bars sub-chart to the bottom of the main chart, using green/red/grey for
    // up/down/flat days
//    m_financeChart->addVolBars(75, 0x99ff99, 0xff9999, 0x808080);

    // Append a 14-days RSI indicator chart (75 pixels high) after the main chart. The main RSI line
    // is purple (800080). Set threshold region to +/- 20 (that is, RSI = 50 +/- 25). The upper/lower
    // threshold regions will be filled with red (ff0000)/blue (0000ff).
    if (m_bars[0])
    {
        m_financeChart->addRSI(75, 14, 0x800080, 20, 0xff0000, 0x0000ff);
    }

    // Append a MACD(26, 12) indicator chart (75 pixels high) after the main chart, using 9 days for
    // computing divergence.
    if (m_bars[1])
    {
        m_financeChart->addMACD(75, 26, 12, 9, 0x0000ff, 0xff00ff, 0x008000);
    }

    // Include track line with legend for the latest data values
    trackFinance(m_financeChart, ((XYChart *)m_financeChart->getChart(0))->getPlotArea()->getRightX());


    // Set the chart image to the QChartView
    delete this->getChart();
    this->setChart(m_financeChart);
    this->createControlBar();
}

//
// Draw track cursor when mouse is moving over plotarea
//
void ChartView::onMouseMovePlotArea(QMouseEvent *)
{
    trackFinance((MultiChart *)this->getChart(), this->getPlotAreaMouseX());
    this->updateDisplay();
}

void ChartView::onTimeout()
{
    drawChart();
}

void ChartView::onControlBarActive(int index)
{
    this->setBar(index, false);
}

//
// Draw finance chart track line with legend
//
void ChartView::trackFinance(MultiChart *m, int mouseX)
{
    // Clear the current dynamic layer and get the DrawArea object to draw on it.
    DrawArea *d = m->initDynamicLayer();

    // It is possible for a FinanceChart to be empty, so we need to check for it.
    if (m->getChartCount() == 0)
        return ;

    // Get the data x-value that is nearest to the mouse
    int xValue = (int)(((XYChart *)m->getChart(0))->getNearestXValue(mouseX));

    // Iterate the XY charts (main price chart and indicator charts) in the FinanceChart
    XYChart *c = 0;
    for(int i = 0; i < m->getChartCount(); ++i) {
        c = (XYChart *)m->getChart(i);

        // Variables to hold the legend entries
        ostringstream ohlcLegend;
        vector<string> legendEntries;

        // Iterate through all layers to find the highest data point
        for(int j = 0; j < c->getLayerCount(); ++j) {
            Layer *layer = c->getLayerByZ(j);
            int xIndex = layer->getXIndexOf(xValue);
            int dataSetCount = layer->getDataSetCount();

            // In a FinanceChart, only layers showing OHLC data can have 4 data sets
            if (dataSetCount == 4) {
                double highValue = layer->getDataSet(0)->getValue(xIndex);
                double lowValue = layer->getDataSet(1)->getValue(xIndex);
                double openValue = layer->getDataSet(2)->getValue(xIndex);
                double closeValue = layer->getDataSet(3)->getValue(xIndex);

                if (closeValue != Chart::NoValue) {
                    // Build the OHLC legend
                    ohlcLegend << "      <*block*>";
                    ohlcLegend << "Open: " << c->formatValue(openValue, "{value|P4}");
                    ohlcLegend << ", High: " << c->formatValue(highValue, "{value|P4}");
                    ohlcLegend << ", Low: " << c->formatValue(lowValue, "{value|P4}");
                    ohlcLegend << ", Close: " << c->formatValue(closeValue, "{value|P4}");

                    // We also draw an upward or downward triangle for up and down days and the %
                    // change
                    double lastCloseValue = layer->getDataSet(3)->getValue(xIndex - 1);
                    if (lastCloseValue != Chart::NoValue) {
                        double change = closeValue - lastCloseValue;
                        double percent = change * 100 / closeValue;
                        string symbol = (change >= 0) ?
                            "<*font,color=008800*><*img=@triangle,width=8,color=008800*>" :
                            "<*font,color=CC0000*><*img=@invertedtriangle,width=8,color=CC0000*>";

                        ohlcLegend << "  " << symbol << " " << c->formatValue(change, "{value|P4}");
                        ohlcLegend << " (" << c->formatValue(percent, "{value|2}") << "%)<*/font*>";
                    }

                    ohlcLegend << "<*/*>";
                }
            } else {
                // Iterate through all the data sets in the layer
                for(int k = 0; k < layer->getDataSetCount(); ++k) {
                    DataSet *dataSet = layer->getDataSetByZ(k);

                    string name = dataSet->getDataName();
                    double value = dataSet->getValue(xIndex);
                    if ((0 != name.size()) && (value != Chart::NoValue)) {

                        // In a FinanceChart, the data set name consists of the indicator name and its
                        // latest value. It is like "Vol: 123M" or "RSI (14): 55.34". As we are
                        // generating the values dynamically, we need to extract the indictor name
                        // out, and also the volume unit (if any).

                        // The volume unit
                        string unitChar;

                        // The indicator name is the part of the name up to the colon character.
                        int delimiterPosition = (int)name.find(':');
                        if ((int)name.npos != delimiterPosition) {

                            // The unit, if any, is the trailing non-digit character(s).
                            int lastDigitPos = (int)name.find_last_of("0123456789");
                            if (((int)name.npos != lastDigitPos) && (lastDigitPos + 1 < (int)name.size())
                                && (lastDigitPos > delimiterPosition))
                                unitChar = name.substr(lastDigitPos + 1);

                            name.resize(delimiterPosition);
                        }

                        // In a FinanceChart, if there are two data sets, it must be representing a
                        // range.
                        if (dataSetCount == 2) {
                            // We show both values in the range in a single legend entry
                            value = layer->getDataSet(0)->getValue(xIndex);
                            double value2 = layer->getDataSet(1)->getValue(xIndex);
                            name = name + ": " + c->formatValue(min(value, value2), "{value|P3}");
                            name = name + " - " + c->formatValue(max(value, value2), "{value|P3}");
                        } else {
                            // In a FinanceChart, only the layer for volume bars has 3 data sets for
                            // up/down/flat days
                            if (dataSetCount == 3) {
                                // The actual volume is the sum of the 3 data sets.
                                value = layer->getDataSet(0)->getValue(xIndex) + layer->getDataSet(1
                                    )->getValue(xIndex) + layer->getDataSet(2)->getValue(xIndex);
                            }

                            // Create the legend entry
                            name = name + ": " + c->formatValue(value, "{value|P3}") + unitChar;
                        }

                        // Build the legend entry, consist of a colored square box and the name (with
                        // the data value in it).
                        ostringstream legendEntry;
                        legendEntry << "<*block*><*img=@square,width=8,edgeColor=000000,color="
                            << hex << dataSet->getDataColor() << "*> " << name << "<*/*>";
                        legendEntries.push_back(legendEntry.str());
                    }
                }
            }
        }

        // Get the plot area position relative to the entire FinanceChart
        PlotArea *plotArea = c->getPlotArea();
        int plotAreaLeftX = plotArea->getLeftX() + c->getAbsOffsetX();
        int plotAreaTopY = plotArea->getTopY() + c->getAbsOffsetY();

        // The legend begins with the date label, then the ohlcLegend (if any), and then the
        // entries for the indicators.
        ostringstream legendText;
        legendText << "<*block,valign=top,maxWidth=" << (plotArea->getWidth() - 5)
            << "*><*font=arialbd.ttf*>[" << c->xAxis()->getFormattedLabel(xValue, "mmm dd, yyyy")
            << "]<*/font*>" << ohlcLegend.str();
        for (int i = ((int)legendEntries.size()) - 1; i >= 0; --i) {
            legendText << "      " << legendEntries[i];
        }
        legendText << "<*/*>";

        // Draw a vertical track line at the x-position
        d->vline(plotAreaTopY, plotAreaTopY + plotArea->getHeight(), c->getXCoor(xValue) +
            c->getAbsOffsetX(), d->dashLineColor(0x000000, 0x0101));

        // Display the legend on the top of the plot area
        TTFText *t = d->text(legendText.str().c_str(), "arial.ttf", 8);
        t->draw(plotAreaLeftX + 5, plotAreaTopY + 3, 0x000000, Chart::TopLeft);
        t->destroy();
    }
}

/////////////////////////////////////////////////////////////////////////////
// Build in mouse cursors for zooming and scrolling support
//

static const int zoomInCursorA[] =
{
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xff3ff8ff,
0xff0fe0ff,
0xff07c0ff,
0xff07c0ff,
0xff0380ff,
0xff0380ff,
0xff0380ff,
0xff0380ff,
0xff0380ff,
0xff07c0ff,
0xff07c0ff,
0xff01e0ff,
0xff30f8ff,
0x7ff0ffff,
0x3ff8ffff,
0x1ffcffff,
0x0ffeffff,
0x0fffffff,
0x9fffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff
};

static const int zoomInCursorB[] =
{
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00c00700,
0x00f01f00,
0x00f01e00,
0x00f83e00,
0x00f83e00,
0x00183000,
0x00f83e00,
0x00f83e00,
0x00f01e00,
0x00f01f00,
0x00c00700,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000
};

static const int zoomOutCursorA[] =
{
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xff3ff8ff,
0xff0fe0ff,
0xff07c0ff,
0xff07c0ff,
0xff0380ff,
0xff0380ff,
0xff0380ff,
0xff0380ff,
0xff0380ff,
0xff07c0ff,
0xff07c0ff,
0xff01e0ff,
0xff30f8ff,
0x7ff0ffff,
0x3ff8ffff,
0x1ffcffff,
0x0ffeffff,
0x0fffffff,
0x9fffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff
};

static const int zoomOutCursorB[] =
{
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00c00700,
0x00f01f00,
0x00f01f00,
0x00f83f00,
0x00f83f00,
0x00183000,
0x00f83f00,
0x00f83f00,
0x00f01f00,
0x00f01f00,
0x00c00700,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000
};

static const int noZoomCursorA[] =
{
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xff3ff8ff,
0xff0fe0ff,
0xff07c0ff,
0xff07c0ff,
0xff0380ff,
0xff0380ff,
0xff0380ff,
0xff0380ff,
0xff0380ff,
0xff07c0ff,
0xff07c0ff,
0xff01e0ff,
0xff30f8ff,
0x7ff0ffff,
0x3ff8ffff,
0x1ffcffff,
0x0ffeffff,
0x0fffffff,
0x9fffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff
};

static const int noZoomCursorB[] =
{
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00c00700,
0x00f01f00,
0x00f01f00,
0x00f83f00,
0x00f83f00,
0x00f83f00,
0x00f83f00,
0x00f83f00,
0x00f01f00,
0x00f01f00,
0x00c00700,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000
};

static int noMove2DCursorA[] =
{
0xffffffff,
0xffffffff,
0xffffffff,
0xff7ffeff,
0xff3ffcff,
0xff1ff8ff,
0xff0ff0ff,
0xff07e0ff,
0xff03c0ff,
0xff03c0ff,
0xfffc3fff,
0x7ffc3ffe,
0x3ffc3ffc,
0x1f7c3ef8,
0x0f3c3cf0,
0x071c38e0,
0x071c38e0,
0x0f3c3cf0,
0x1f7c3ef8,
0x3ffc3ffc,
0x7ffc3ffe,
0xfffc3fff,
0xff03c0ff,
0xff03c0ff,
0xff07e0ff,
0xff0ff0ff,
0xff1ff8ff,
0xff3ffcff,
0xff7ffeff,
0xffffffff,
0xffffffff,
0xffffffff
};

static int noMove2DCursorB[] =
{
0x00000000,
0x00000000,
0x00000000,
0x00800100,
0x00400200,
0x00200400,
0x00100800,
0x00081000,
0x00042000,
0x00fc3f00,
0x0003c000,
0x80024001,
0x40024002,
0x20824104,
0x10424208,
0x08224410,
0x08224410,
0x10424208,
0x20824104,
0x40024002,
0x80024001,
0x0003c000,
0x00fc3f00,
0x00042000,
0x00081000,
0x00100800,
0x00200400,
0x00400200,
0x00800100,
0x00000000,
0x00000000,
0x00000000
};

static int noMoveHorizCursorA[] =
{
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xfffc3fff,
0x7ffc3ffe,
0x3ffc3ffc,
0x1f7c3ef8,
0x0f3c3cf0,
0x071c38e0,
0x071c38e0,
0x0f3c3cf0,
0x1f7c3ef8,
0x3ffc3ffc,
0x7ffc3ffe,
0xfffc3fff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff,
0xffffffff
};

static int noMoveHorizCursorB[] =
{
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x0003c000,
0x80024001,
0x40024002,
0x20824104,
0x10424208,
0x08224410,
0x08224410,
0x10424208,
0x20824104,
0x40024002,
0x80024001,
0x0003c000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000,
0x00000000
};

static int noMoveVertCursorA[] =
{
0xffffffff,
0xffffffff,
0xffffffff,
0xff7ffeff,
0xff3ffcff,
0xff1ff8ff,
0xff0ff0ff,
0xff07e0ff,
0xff03c0ff,
0xff03c0ff,
0xffffffff,
0xffffffff,
0xffffffff,
0xff7ffeff,
0xff3ffcff,
0xff1ff8ff,
0xff1ff8ff,
0xff3ffcff,
0xff7ffeff,
0xffffffff,
0xffffffff,
0xffffffff,
0xff03c0ff,
0xff03c0ff,
0xff07e0ff,
0xff0ff0ff,
0xff1ff8ff,
0xff3ffcff,
0xff7ffeff,
0xffffffff,
0xffffffff,
0xffffffff
};

static int noMoveVertCursorB[] =
{
0x00000000,
0x00000000,
0x00000000,
0x00800100,
0x00400200,
0x00200400,
0x00100800,
0x00081000,
0x00042000,
0x00fc3f00,
0x00000000,
0x00000000,
0x00000000,
0x00800100,
0x00400200,
0x00200400,
0x00200400,
0x00400200,
0x00800100,
0x00000000,
0x00000000,
0x00000000,
0x00fc3f00,
0x00042000,
0x00081000,
0x00100800,
0x00200400,
0x00400200,
0x00800100,
0x00000000,
0x00000000,
0x00000000
};

static QCursor *hZoomInCursor = 0;
static QCursor *hZoomOutCursor = 0;
static QCursor *hNoZoomCursor = 0;
static QCursor *hNoMove2DCursor = 0;
static QCursor *hNoMoveHorizCursor = 0;
static QCursor *hNoMoveVertCursor = 0;

class FreeCursors
{
public:
    ~FreeCursors()
    {
        delete hZoomInCursor;
        delete hZoomOutCursor;
        delete hNoZoomCursor;
        delete hNoMove2DCursor;
        delete hNoMoveHorizCursor;
        delete hNoMoveVertCursor;
    }
} dummyFreeCursorObj;

static QCursor *createCursor(const int *andPlane, const int *orPlane, int hotX, int hotY)
{
    // The cursor bitmaps are in and/or plane formats used in the Win32 CreateCursor
    // function. We need to change it to the bitmap/mask format used by QT.
    int buffer[32];
    for (int i = 0; i < 32; ++i)
        buffer[i] = ~(orPlane[i] + andPlane[i]);
    QBitmap bitmap = QBitmap::fromData(QSize(32, 32), (uchar *)buffer, QImage::Format_Mono);
    for (int i = 0; i < 32; ++i)
        buffer[i] = orPlane[i] | ~andPlane[i];
    QBitmap mask = QBitmap::fromData(QSize(32, 32), (uchar *)buffer, QImage::Format_Mono);

    return new QCursor(bitmap, mask, hotX, hotY);
}

static QCursor &getZoomInCursor()
{
    if (0 == hZoomInCursor)
        hZoomInCursor = createCursor(zoomInCursorA, zoomInCursorB, 15, 15);
    return *hZoomInCursor;
}

static QCursor &getZoomOutCursor()
{
    if (0 == hZoomOutCursor)
        hZoomOutCursor = createCursor(zoomOutCursorA, zoomOutCursorB, 15, 15);
    return *hZoomOutCursor;
}

static QCursor &getNoZoomCursor()
{
    if (0 == hNoZoomCursor)
        hNoZoomCursor = createCursor(noZoomCursorA, noZoomCursorB, 15, 15);
    return *hNoZoomCursor;
}

static QCursor &getNoMove2DCursor()
{
    if (0 == hNoMove2DCursor)
        hNoMove2DCursor = createCursor(noMove2DCursorA, noMove2DCursorB, 15, 15);
    return *hNoMove2DCursor;
}

static QCursor &getNoMoveHorizCursor()
{
    if (0 == hNoMoveHorizCursor)
        hNoMoveHorizCursor = createCursor(noMoveHorizCursorA, noMoveHorizCursorB, 15, 15);
    return *hNoMoveHorizCursor;
}

static QCursor &getNoMoveVertCursor()
{
    if (0 == hNoMoveVertCursor)
        hNoMoveVertCursor = createCursor(noMoveVertCursorA, noMoveVertCursorB, 15, 15);
    return *hNoMoveVertCursor;
}
