#include "tickerdelegate.h"

#include <QDebug>

TickerDelegate::TickerDelegate()
{
    m_itemWidget = new TickerItemWidget();
}

void TickerDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Ticker* item = index.data().value<Ticker*>();

    m_itemWidget->setName(item->name());
    m_itemWidget->setPrice(item->price());

    QPalette pal;

    if ((option.state & QStyle::State_Selected) == QStyle::State_Selected) {
        pal.setBrush(QPalette::Window, QBrush(QColor(255, 180, 55)));
    }
    else {
        //if (index.row() % 2 == 0) {
            pal.setBrush(QPalette::Window, QBrush(Qt::white));
        //} else {
        //    pal.setBrush(QPalette::Window, QBrush(QColor(227, 232, 237)));
        //}
    }

    m_itemWidget->setPalette(pal);
    m_itemWidget->resize(option.rect.size());

    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->translate(option.rect.topLeft());
    m_itemWidget->render(painter);

    painter->restore();
}

QSize TickerDelegate::sizeHint(const QStyleOptionViewItem&, const QModelIndex&) const
{
    return m_itemWidget->sizeHint();
}
