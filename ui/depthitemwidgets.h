#ifndef TRADEITEMWIDGETS_H
#define TRADEITEMWIDGETS_H
#include <QWidget>
#include <QLabel>

#include "core/depthoffer.h"

class DepthItemWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DepthItemWidget(QWidget *parent = 0);
    virtual void initWithDepth(DepthOffer* pDepthOffer) = 0;
};


class SaleDepthItemWidget : public DepthItemWidget
{
    Q_OBJECT

public:
    explicit SaleDepthItemWidget(QWidget *parent = 0);
    virtual void initWithDepth(DepthOffer* pDepthOffer);

private:
    QLabel* m_sale;
};

class PriceDepthItemWidget : public DepthItemWidget
{
    Q_OBJECT

public:
    explicit PriceDepthItemWidget(QWidget *parent = 0);
    virtual void initWithDepth(DepthOffer* pDepthOffer);

private:
    QLabel* m_price;
};

class PurchaseDepthItemWidget : public DepthItemWidget
{
    Q_OBJECT

public:
    explicit PurchaseDepthItemWidget(QWidget *parent = 0);
    virtual void initWithDepth(DepthOffer* pDepthOffer);

private:
    QLabel* m_purchase;
};

#endif // TRADEITEMWIDGETS_H
