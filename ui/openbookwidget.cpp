#include "openbookwidget.h"

#include <QTableView>
#include <QHeaderView>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QDoubleSpinBox>

#include "openbookitemdelegate.h"

OpenBookWidget::OpenBookWidget(const QString &tickerName, QWidget *parent) :
    QWidget(parent),
    m_tickerName(tickerName)
{
    QVBoxLayout* pVBoxLayout = new QVBoxLayout(this);
    QLabel* pVolFrom = new QLabel(this);
    pVolFrom->setText("Vol from");

    QDoubleSpinBox* pSpinFrom = new QDoubleSpinBox(this);
    pSpinFrom->setMaximum(99999);
    pSpinFrom->setSingleStep(0.1);
    connect(pSpinFrom, SIGNAL(valueChanged(double)), this, SLOT(SortMinAmountChanged(double)));

    QDoubleSpinBox* pSpinTo = new QDoubleSpinBox(this);
    pSpinTo->setMaximum(99999);
    pSpinTo->setSingleStep(0.1);
    connect(pSpinTo, SIGNAL(valueChanged(double)), this, SLOT(SortMaxAmountChanged(double)));

    QLabel* pVolTo = new QLabel(this);
    pVolTo->setText("Vol to");

    QHBoxLayout* pHLayout = new QHBoxLayout(this);
    pHLayout->addWidget(pVolFrom);
    pHLayout->addWidget(pSpinFrom);
    pHLayout->addWidget(pVolTo);
    pHLayout->addWidget(pSpinTo);
    pVBoxLayout->addLayout(pHLayout);


    QTableView* pTableView = new QTableView(this);
    pVBoxLayout->addWidget(pTableView);
    pTradeModel = SBitApi::instance()->depthModel(tickerName);


    m_pDepthFilterModel.setDynamicSortFilter(true);
    m_pDepthFilterModel.setSourceModel(pTradeModel);

    pTableView->setStyleSheet("QHeaderView::section {"
                              "  color: rgba(136, 144, 152, 255);"
                              "  font: bold 14px;"
                              "  }");

    pTableView->setModel(&m_pDepthFilterModel);
    pTableView->setItemDelegate(new OpenBookItemDelegate(this));
    setWindowTitle(QObject::trUtf8("Стакан ") + tickerName);
    pTableView->verticalHeader()->hide();
    pTableView->horizontalHeader()->setHighlightSections(false);
    pTableView->setSelectionMode(QAbstractItemView::SingleSelection);
    pTableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    pTableView->setSelectionModel(pTradeModel->selectionModel());
    setLayout(pVBoxLayout);
}

OpenBookWidget::~OpenBookWidget()
{

}

void OpenBookWidget::SortMinAmountChanged(double minAmount)
{
    m_pDepthFilterModel.setAmountMin(minAmount);
    m_pDepthFilterModel.setSortedByAmount(true);
}

void OpenBookWidget::SortMaxAmountChanged(double maxAmount)
{
    m_pDepthFilterModel.setAmountMax(maxAmount);
    m_pDepthFilterModel.setSortedByAmount(true);
}

void OpenBookWidget::SortMinPriceChanged(double minAmount)
{
    m_pDepthFilterModel.setPriceMin(minAmount);
    m_pDepthFilterModel.setSortedByPrice(true);
}

void OpenBookWidget::SortMaxPriceChanged(double maxAmount)
{
    m_pDepthFilterModel.setPriceMax(maxAmount);
    m_pDepthFilterModel.setSortedByPrice(true);
}

void OpenBookWidget::SortAmountFlagChanged(bool flag)
{
    m_pDepthFilterModel.setSortedByAmount(flag);
}

void OpenBookWidget::SortPriceFlagChanged(bool flag)
{
    m_pDepthFilterModel.setSortedByPrice(flag);
}
