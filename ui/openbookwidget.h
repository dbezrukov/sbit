#ifndef OPENBOOKWIDGET_H
#define OPENBOOKWIDGET_H

#include <QWidget>

#include "api/sbitapi.h"
#include "core/depthfiltermodel.h"

class OpenBookWidget : public QWidget
{
    Q_OBJECT
public:
    explicit OpenBookWidget(const QString& tickerName, QWidget *parent = 0);
    ~OpenBookWidget();

signals:
    
public slots:
    void SortMinAmountChanged(double minAmount);
    void SortMaxAmountChanged(double maxAmount);

    void SortMinPriceChanged(double minAmount);
    void SortMaxPriceChanged(double maxAmount);

    void SortAmountFlagChanged(bool flag);
    void SortPriceFlagChanged(bool flag);
    
private:
    QString m_tickerName;
    DepthModel* pTradeModel;
    DepthFilterModel m_pDepthFilterModel;
};

#endif // OPENBOOKWIDGET_H
