#ifndef TICKERITEMWIDGETCHARTS_H
#define TICKERITEMWIDGETCHARTS_H

#include <QWidget>
#include <QLabel>

class TickerItemWidgetCharts : public QWidget
{
    Q_OBJECT

public:
    explicit TickerItemWidgetCharts(QWidget *parent = 0);
    void setName(QString);
    void setSelected(bool);
    QSize sizeHint() const;

protected:
    QLabel* m_name;
};


#endif // TICKERITEMWIDGETCHARTS_H
