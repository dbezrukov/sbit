#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QWidget>

namespace Ui {
class LoginWindow;
}

class LoginWindow : public QWidget
{
    Q_OBJECT
    
public:
    explicit LoginWindow(QString, QString, QWidget *parent = 0);
    ~LoginWindow();
    
private slots:
    void on_buttonCancel_clicked();
    void on_buttonOk_clicked();

signals:
    void cancel();
    void login(QString, QString);

private:
    Ui::LoginWindow *ui;
};

#endif // LOGINWINDOW_H
