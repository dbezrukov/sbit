#ifndef TABHISTORY_H
#define TABHISTORY_H

#include <QWidget>
#include <QModelIndex>

namespace Ui {
class TabHistory;
}

class TabHistory : public QWidget
{
    Q_OBJECT

public:
    explicit TabHistory(QWidget *parent = 0);
    ~TabHistory();

private:
    Ui::TabHistory *ui;
};

#endif // TABHISTORY_H
