#include "tickerdelegatecharts.h"

#include <QDebug>

TickerDelegateCharts::TickerDelegateCharts()
{
    m_itemWidget = new TickerItemWidgetCharts();
}

void TickerDelegateCharts::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Ticker* item = index.data().value<Ticker*>();

    m_itemWidget->setName(item->name());

    QPalette pal;
    pal.setBrush(QPalette::Window, QBrush(QColor(Qt::transparent)));
    m_itemWidget->setPalette(pal);

    m_itemWidget->resize(option.rect.size());
    m_itemWidget->setSelected((option.state & QStyle::State_Selected) == QStyle::State_Selected);

    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->translate(option.rect.topLeft());
    m_itemWidget->render(painter);

    painter->restore();
}

QSize TickerDelegateCharts::sizeHint(const QStyleOptionViewItem&, const QModelIndex&) const
{
    return m_itemWidget->sizeHint();
}
