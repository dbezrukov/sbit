#include "depthitemwidgets.h"
#include <QHBoxLayout>

SaleDepthItemWidget::SaleDepthItemWidget(QWidget *parent) :
    DepthItemWidget(parent)
{
    m_sale = new QLabel(this);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins(10,0,4,0));
    layout->addWidget(m_sale);

    QFont font;
    m_sale->setFont(font);

    QPalette palette = m_sale->palette();
    palette.setColor(m_sale->foregroundRole(), Qt::red);
    m_sale->setPalette(palette);


    //setFixedHeight(25);
}

void SaleDepthItemWidget::initWithDepth(DepthOffer *pDepthOffer)
{
    if (pDepthOffer->sell())
        m_sale->setText(QString::number(pDepthOffer->amount()));
    else
        m_sale->setText("");
}


PriceDepthItemWidget::PriceDepthItemWidget(QWidget *parent) :
    DepthItemWidget(parent)
{
    m_price = new QLabel(this);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins(10,0,4,0));
    layout->addWidget(m_price);

    QFont font;
    m_price->setFont(font);

    //setFixedHeight(25);
}

void PriceDepthItemWidget::initWithDepth(DepthOffer *pDepthOffer)
{
    m_price->setText(QString::number(pDepthOffer->price()));
}


PurchaseDepthItemWidget::PurchaseDepthItemWidget(QWidget *parent) :
    DepthItemWidget(parent)
{
    m_purchase = new QLabel(this);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins(10,0,4,0));
    layout->addWidget(m_purchase);

    QFont font;
    m_purchase->setFont(font);

    QPalette palette = m_purchase->palette();
    palette.setColor(m_purchase->foregroundRole(), Qt::darkGreen);
    m_purchase->setPalette(palette);

    //setFixedHeight(25);
}

void PurchaseDepthItemWidget::initWithDepth(DepthOffer *pDepthOffer)
{
    if (!pDepthOffer->sell())
        m_purchase->setText(QString::number(pDepthOffer->amount()));
    else
        m_purchase->setText("");
}


DepthItemWidget::DepthItemWidget(QWidget *parent) :
    QWidget(parent)
{
}
