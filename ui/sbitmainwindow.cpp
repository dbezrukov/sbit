#include "sbitmainwindow.h"
#include "ui_sbitmainwindow.h"
#include "tabmain.h"
#include "tabchart.h"
#include "tabhistory.h"
#include "taborders.h"
#include "api/sbitapi.h"

#include <QDesktopServices>
#include <QPushButton>
#include <QMenu>
#include <QUrl>
#include <QAction>

SBitMainWindow::SBitMainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::SBitMainWindow)
{
    SBitApi::instance()->setParent(this);

    ui->setupUi(this);

    // clear form-generated tabs
    ui->tabWidgetTop->clear();
    ui->tabWidgetBottom->clear();

    // add tabs
    ui->tabWidgetTop->addTab(createTabMain(), trUtf8("Главная"));

    //ui->tabWidgetBottom->addTab(createTabChart(), trUtf8("Графики"));
    ui->tabWidgetBottom->addTab(createTabOrders(), trUtf8("   Активные сделки  "));
    ui->tabWidgetBottom->addTab(createTabHistory(), trUtf8("  История сделок  "));

    // add file button
    QPushButton* buttonFile = new QPushButton(trUtf8("Файл"), ui->tabWidgetTop);
    buttonFile->move(7, 10);
    buttonFile->setObjectName("buttonFile");
    buttonFile->setStyleSheet("#buttonFile{"
                                  "border: 1px solid #8f8f91;"
                                  "color: rgba(248, 255, 214, 255);"
                                  "border-radius: 6px;"
                                  "background-color: rgba(248, 141, 13, 255, 255);"
                                  "min-width: 80px;"
                                  "min-height: 21px;"
                               "}"
                               "#buttonFile:hover{"
                                  "border: 1px solid rgba(216, 107, 13, 255);"
                               "}"
                               "#buttonFile:pressed{"
                                  "background-color: rgba(216, 107, 13, 255)"
                               "}");

    // add file menu
    QMenu* menu   = new QMenu();

    QAction* actionAccountConfig = new QAction(trUtf8("Управление счётом"), menu);
    QAction* actionSignout = new QAction(trUtf8("Завершение сеанса"), menu);
    QAction* actionExit = new QAction(trUtf8("Выход"), menu);

    menu->addAction(actionAccountConfig);
    menu->addSeparator();
    menu->addAction(actionSignout);
    menu->addAction(actionExit);

    buttonFile->setMenu(menu);

    connect(actionAccountConfig, SIGNAL(triggered()), this, SLOT(actionAccountConfig()));
    connect(actionSignout, SIGNAL(triggered()), this, SLOT(actionSignout()));
    connect(actionExit, SIGNAL(triggered()), this, SLOT(actionExit()));
}

SBitMainWindow::~SBitMainWindow()
{
    delete ui;
}

QWidget *SBitMainWindow::createTabMain()
{
    QWidget* tabMain = new TabMain();
    return tabMain;
}

QWidget *SBitMainWindow::createTabOrders()
{
    QWidget* tabOrders= new TabOrders();
    return tabOrders;
}

QWidget *SBitMainWindow::createTabChart()
{
    QWidget* tabChart = new TabChart();
    return tabChart;
}

QWidget *SBitMainWindow::createTabHistory()
{
    QWidget* tabHistory = new TabHistory();
    return tabHistory;
}

void SBitMainWindow::showWindow(QString key, QString secret)
{
    SBitApi::instance()->setCredentials(key, secret);
    showMaximized();
}

void SBitMainWindow::actionAccountConfig()
{
    QDesktopServices::openUrl(QUrl("http://upbit.org"));
}

void SBitMainWindow::actionSignout()
{
    close();
}

void SBitMainWindow::actionExit()
{
    close();
}
