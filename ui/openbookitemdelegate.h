#ifndef OPENBOOKITEMDELEGATE_H
#define OPENBOOKITEMDELEGATE_H

#include <QItemDelegate>

class OpenBookItemDelegate : public QItemDelegate
{
public:
    OpenBookItemDelegate(QObject *parent = 0);
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // OPENBOOKITEMDELEGATE_H
