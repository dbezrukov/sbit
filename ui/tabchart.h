#ifndef TABCHART_H
#define TABCHART_H

#include <QWidget>
#include <QModelIndex>

#include "tickerchartwidget.h"

namespace Ui {
class TabChart;
}

class TabChart : public QWidget
{
    Q_OBJECT
    
public:
    explicit TabChart(QWidget *parent = 0);
    ~TabChart();

protected:
    TickerChartWidget* chartForTicker(QString);

protected slots:
    void currentTickerChanged(QModelIndex,QModelIndex);

private:
    Ui::TabChart *ui;
};

#endif // TABCHART_H
