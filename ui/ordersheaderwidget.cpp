#include "ordersheaderwidget.h"
#include "ui_ordersheaderwidget.h"

OrdersHeaderWidget::OrdersHeaderWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::OrdersHeaderWidget)
{
    ui->setupUi(this);
}

OrdersHeaderWidget::~OrdersHeaderWidget()
{
    delete ui;
}
