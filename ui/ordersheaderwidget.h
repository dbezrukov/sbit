#ifndef ORDERSHEADERWIDGET_H
#define ORDERSHEADERWIDGET_H

#include <QWidget>

namespace Ui {
class OrdersHeaderWidget;
}

class OrdersHeaderWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit OrdersHeaderWidget(QWidget *parent = 0);
    ~OrdersHeaderWidget();
    
private:
    Ui::OrdersHeaderWidget *ui;
};

#endif // ORDERSHEADERWIDGET_H
