#include "orderitemwidgets.h"
#include <QHBoxLayout>

OrderItemWidget::OrderItemWidget(QWidget *parent) :
    QWidget(parent)
{
}

ActionOrderItemWidget::ActionOrderItemWidget(QWidget *parent) :
    OrderItemWidget(parent)
{
    m_action = new QLabel(this);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins(10,0,4,0));
    layout->addWidget(m_action);

    QFont font;
    m_action->setFont(font);

    QPalette palette = m_action->palette();
    palette.setColor(m_action->foregroundRole(), Qt::black);
    m_action->setPalette(palette);
}

void ActionOrderItemWidget::initWithOrder(Order *pOrder)
{
    if (pOrder->sell())
        m_action->setText(trUtf8("Продажа"));
    else
        m_action->setText(trUtf8("Покупка"));
}

PairOrderItemWidget::PairOrderItemWidget(QWidget *parent) :
    OrderItemWidget(parent)
{
    m_pair = new QLabel(this);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins(10,0,4,0));
    layout->addWidget(m_pair);

    QFont font;
    m_pair->setFont(font);

    QPalette palette = m_pair->palette();
    palette.setColor(m_pair->foregroundRole(), Qt::black);
    m_pair->setPalette(palette);
}

void PairOrderItemWidget::initWithOrder(Order *pOrder)
{
    m_pair->setText(pOrder->tickerName());
}

PriceOrderItemWidget::PriceOrderItemWidget(QWidget *parent) :
    OrderItemWidget(parent)
{
    m_price = new QLabel(this);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins(10,0,4,0));
    layout->addWidget(m_price);

    QFont font;
    m_price->setFont(font);

    QPalette palette = m_price->palette();
    palette.setColor(m_price->foregroundRole(), Qt::black);
    m_price->setPalette(palette);
}

void PriceOrderItemWidget::initWithOrder(Order *pOrder)
{
    m_price->setText(QString::number(pOrder->rate()));
}

QntyOrderItemWidget::QntyOrderItemWidget(QWidget *parent) :
    OrderItemWidget(parent)
{
    m_qnty = new QLabel(this);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins(10,0,4,0));
    layout->addWidget(m_qnty);

    QFont font;
    m_qnty->setFont(font);

    QPalette palette = m_qnty->palette();
    palette.setColor(m_qnty->foregroundRole(), Qt::black);
    m_qnty->setPalette(palette);
}

void QntyOrderItemWidget::initWithOrder(Order *pOrder)
{
    m_qnty->setText(QString::number(pOrder->amount()));
}

SummOrderItemWidget::SummOrderItemWidget(QWidget *parent) :
    OrderItemWidget(parent)
{
    m_summ = new QLabel(this);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins(10,0,4,0));
    layout->addWidget(m_summ);

    QFont font;
    m_summ->setFont(font);

    QPalette palette = m_summ->palette();
    palette.setColor(m_summ->foregroundRole(), Qt::black);
    m_summ->setPalette(palette);
}

void SummOrderItemWidget::initWithOrder(Order *pOrder)
{
    double cost = pOrder->rate() * pOrder->amount();
    m_summ->setText(QString::number(cost));
}

StatusOrderItemWidget::StatusOrderItemWidget(QWidget *parent) :
    OrderItemWidget(parent)
{
    m_status = new QLabel(this);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(QMargins(10,0,4,0));
    layout->addWidget(m_status);

    QFont font;
    m_status->setFont(font);

    QPalette palette = m_status->palette();
    palette.setColor(m_status->foregroundRole(), Qt::black);
    m_status->setPalette(palette);
}

void StatusOrderItemWidget::initWithOrder(Order *pOrder)
{
    m_status->setText(pOrder->statusString());
}
