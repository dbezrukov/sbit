#ifndef ORDERITEMWIDGETS_H
#define ORDERITEMWIDGETS_H

#include <QWidget>
#include <QLabel>

#include "core/order.h"

class OrderItemWidget : public QWidget
{
    Q_OBJECT
public:
    explicit OrderItemWidget(QWidget *parent = 0);
    virtual void initWithOrder(Order* pOrder) = 0;
};

class ActionOrderItemWidget : public OrderItemWidget
{
    Q_OBJECT

public:
    explicit ActionOrderItemWidget(QWidget *parent = 0);
    virtual void initWithOrder(Order* pOrder);

private:
    QLabel* m_action;
};

class PairOrderItemWidget : public OrderItemWidget
{
    Q_OBJECT

public:
    explicit PairOrderItemWidget(QWidget *parent = 0);
    virtual void initWithOrder(Order* pOrder);

private:
    QLabel* m_pair;
};

class PriceOrderItemWidget : public OrderItemWidget
{
    Q_OBJECT

public:
    explicit PriceOrderItemWidget(QWidget *parent = 0);
    virtual void initWithOrder(Order* pOrder);

private:
    QLabel* m_price;
};

class QntyOrderItemWidget : public OrderItemWidget
{
    Q_OBJECT

public:
    explicit QntyOrderItemWidget(QWidget *parent = 0);
    virtual void initWithOrder(Order* pOrder);

private:
    QLabel* m_qnty;
};

class SummOrderItemWidget : public OrderItemWidget
{
    Q_OBJECT

public:
    explicit SummOrderItemWidget(QWidget *parent = 0);
    virtual void initWithOrder(Order* pOrder);

private:
    QLabel* m_summ;
};

class StatusOrderItemWidget : public OrderItemWidget
{
    Q_OBJECT

public:
    explicit StatusOrderItemWidget(QWidget *parent = 0);
    virtual void initWithOrder(Order* pOrder);

private:
    QLabel* m_status;
};

#endif // ORDERITEMWIDGETS_H
