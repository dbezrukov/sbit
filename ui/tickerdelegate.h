#ifndef TICKERDELEGATE_H
#define TICKERDELEGATE_H

#include <QItemDelegate>
#include <QPainter>

#include "tickeritemwidget.h"
#include "core/ticker.h"

class TickerDelegate : public QItemDelegate
{
public:
    TickerDelegate();
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

protected:
    TickerItemWidget* m_itemWidget;
};

#endif // TICKERDELEGATE_H
