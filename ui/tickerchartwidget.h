#ifndef TICKERCHARTWIDGET_H
#define TICKERCHARTWIDGET_H

#include <QWidget>
#include <QResizeEvent>

#include "core/ticker.h"
#include "ui/chartview.h"

namespace Ui {
class TickerChartWidget;
}

class TickerChartWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit TickerChartWidget(Ticker*, QWidget *parent = 0);
    ~TickerChartWidget();
    QString tickerName();
    
    void resizeEvent(QResizeEvent *e);

private:
    Ui::TickerChartWidget *ui;
    ChartView *m_chartView;
    Ticker* m_ticker;   

private slots:
    void insertBar();
    void deleteBar();
};

#endif // TICKERCHARTWIDGET_H
