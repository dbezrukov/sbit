#include "loginwindow.h"
#include "ui_loginwindow.h"

LoginWindow::LoginWindow(QString defaultKey, QString defaultSecret, QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::LoginWindow)
{
    ui->setupUi(this);

    ui->editKey->setText(defaultKey);
    ui->editSecret->setText(defaultSecret);
}

LoginWindow::~LoginWindow()
{
    delete ui;
}

void LoginWindow::on_buttonCancel_clicked()
{
    emit cancel();
    close();
}

void LoginWindow::on_buttonOk_clicked()
{
    if (ui->editKey->text().isEmpty() || ui->editSecret->text().isEmpty())
        return;

    emit login(ui->editKey->text(), ui->editSecret->text());
    close();
}
