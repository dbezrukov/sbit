#include "tabchart.h"
#include "ui_tabchart.h"
#include "api/sbitapi.h"
#include "ui/tickerdelegatecharts.h"

#include <QDebug>

TabChart::TabChart(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::TabChart)
{
    ui->setupUi(this);

    ui->tickersList->setModel(SBitApi::instance()->tickers());
    ui->tickersList->setSelectionModel(SBitApi::instance()->tickers()->selectionModel());
    ui->tickersList->setItemDelegate(new TickerDelegateCharts);
    connect(ui->tickersList->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this, SLOT(currentTickerChanged(QModelIndex,QModelIndex)));
    ui->tickersList->setCurrentIndex(ui->tickersList->model()->index(0, 0));
}

TabChart::~TabChart()
{
    delete ui;
}

TickerChartWidget *TabChart::chartForTicker(QString tickerName)
{
    for (int i=0; i<ui->stackedWidget->count(); i++)
    {
        TickerChartWidget* chart = qobject_cast<TickerChartWidget*>(ui->stackedWidget->widget(i));
        if (chart && chart->tickerName() == tickerName) {
            qDebug() << chart->tickerName();
            qDebug() << tickerName;
            return chart;
        }
    }
    return 0;
}

void TabChart::currentTickerChanged(QModelIndex current, QModelIndex)
{
    Ticker* ticker = current.data().value<Ticker*>();
    TickerChartWidget* chart = chartForTicker(ticker->name());

    if (chart) {
        ui->stackedWidget->setCurrentWidget(chart);
    }
    else {
        int index = ui->stackedWidget->addWidget(new TickerChartWidget(ticker));
        ui->stackedWidget->setCurrentIndex(index);
    }
}
