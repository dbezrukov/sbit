#include "depthoffer.h"

DepthOffer::DepthOffer(QObject *parent) :
    QObject(parent)
{
}

double DepthOffer::price()
{
    return m_price;
}

void DepthOffer::setPrice(double price)
{
    m_price = price;
}

double DepthOffer::amount()
{
    return m_amount;
}

void DepthOffer::setAmount(double amount)
{
    m_amount = amount;
}

bool DepthOffer::sell()
{
    return m_sell;
}

void DepthOffer::setSell(bool sell)
{
    m_sell = sell;
}

bool DepthOffer::operator ==(const DepthOffer &rh)
{
    return ((rh.m_amount == m_amount) &&
            (rh.m_price == m_price));
}

bool DepthOffer::operator <(const DepthOffer &rh)
{
    if (m_sell && !rh.m_sell)
        return false;
    if (!m_sell && rh.m_sell)
        return true;

    return (m_price < rh.m_price);
}

bool DepthOffer::IsInList(const QList<DepthOffer *> &offersList)
{
    foreach (DepthOffer* depthOffer, offersList)
    {
        if (*depthOffer == *this)
            return true;
    }
    return false;
}

bool DepthOffer::IsInList(const QList<DepthOffer *> &offersList, int maxCheckIndex)
{
    int ind = 0;
    foreach (DepthOffer* depthOffer, offersList)
    {
        if (ind > maxCheckIndex)
            break;
        if (*depthOffer == *this)
            return true;
        ++ind;
    }
    return false;
}

int DepthOffer::IndexInList(const QList<DepthOffer *> &offersList)
{
    int i = 0;
    foreach (DepthOffer* depthOffer, offersList)
    {
        if (*depthOffer == *this)
            return i;
        ++i;
    }
    return -1;
}


template<class T>
PCompareByVal<T>::PCompareByVal(T *pT) :
    m_pT(pT)
{

}

template<class T>
T *PCompareByVal<T>::getPointer()
{
    return m_pT;
}

template<class T>
T &PCompareByVal<T>::operator ->()
{
    return *m_pT;
}

template<class T>
bool PCompareByVal<T>::operator ==(const PCompareByVal &rh)
{
    return (*m_pT == rh);
}
