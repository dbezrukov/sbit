#include "order.h"

Order::Order(qlonglong id, qlonglong timestampCreated, QObject *parent)
    : QObject(parent)
    , m_id(id)
    , m_timestampCreated(timestampCreated)
    , m_rate(0.f)
    , m_amount(0.f)
    , m_status(0)
{
}

qlonglong Order::id()
{
    return m_id;
}

qlonglong Order::timestampCreated()
{
    return m_timestampCreated;
}

void Order::setTickerName(QString tickerName)
{
    m_tickerName = tickerName;
    emit orderInfoUpdated(this);
}

QString Order::tickerName()
{
    return m_tickerName;
}

void Order::setSell(bool sell)
{
    m_sell = sell;
    emit orderInfoUpdated(this);
}

bool Order::sell()
{
    return m_sell;
}

void Order::setRate(double rate)
{
    m_rate = rate;
    emit orderInfoUpdated(this);
}

double Order::rate()
{
    return m_rate;
}

void Order::setStatus(int status)
{
    m_status = status;
    emit orderInfoUpdated(this);
}

int Order::status()
{
    return m_status;
}

QString Order::statusString()
{
    switch (m_status)
    {
        case 0:
            return trUtf8("Активна");
        case 1:
            return trUtf8("Исполнена");
        case 2:
            return trUtf8("Отменена");
        default: ;
    }
    Q_ASSERT_X(0, Q_FUNC_INFO, "Should't be here");
    return QString();
}

void Order::setamount(double amount)
{
    m_amount = amount;
    emit orderInfoUpdated(this);
}

double Order::amount()
{
    return m_amount;
}
