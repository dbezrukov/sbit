#ifndef OHLCDATA_H
#define OHLCDATA_H

#include <QObject>
#include <QMetaType>

#include "common.h"

class OhlcData : public QObject
{
    Q_OBJECT

public:
    explicit OhlcData(QObject *parent = 0);

    void setData(qlonglong timestamp, double priceOpen, double priceHi, double priceLow, double priceClose, double volume);

    double priceOpen();
    double priceHi();
    double priceLow();
    double priceClose();
    double volume();
    double timestamp();

signals:
    void ohlcInfoUpdated(OhlcData*);

protected:
    double m_priceOpen;
    double m_priceHi;
    double m_priceLow;
    double m_priceClose;
    double m_volume;
    qlonglong m_timestamp;
};

Q_DECLARE_METATYPE(OhlcData*)

#endif // OHLC_H
