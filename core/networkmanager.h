#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <QNetworkAccessManager>
#include <QQueue>
#include <QPair>
#include <QTimer>

class AbstractRequest;

class NetworkManager : public QNetworkAccessManager
{
    Q_OBJECT

public:
    explicit NetworkManager(const QString& key, const QString& secret, QObject *parent = 0);
    void enqueue(AbstractRequest* request, const QString& requestString);
    void sendRequest(AbstractRequest* request, const QString& requestString);
    void setNonce(qlonglong);

protected:
    QString hmac(const QString& data, const QString& secret);

protected slots:
    void sendQueuedRequest();

protected:
    QString m_key;
    QString m_secret;
    QQueue<QPair<AbstractRequest*, QString> > m_queue;
    QTimer* m_sendingTimer;
    qlonglong m_nonce;
};

#endif // NETWORKMANAGER_H
