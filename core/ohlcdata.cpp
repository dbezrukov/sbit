#include "ohlcdata.h"

OhlcData::OhlcData(QObject *parent)
    : QObject(parent)
    , m_priceOpen(0.f)
    , m_priceHi(0.f)
    , m_priceLow(0.f)
    , m_priceClose(0.f)
    , m_timestamp(0ll)
{
}

void OhlcData::setData(qlonglong timestamp, double priceOpen, double priceHi, double priceLow, double priceClose, double volume)
{
    m_priceOpen = priceOpen;
    m_priceHi = priceHi;
    m_priceLow = priceLow;
    m_priceClose = priceClose;
    m_timestamp = timestamp;
    m_volume = volume;

    emit ohlcInfoUpdated(this);
}

double OhlcData::priceOpen()
{
    return m_priceOpen;
}

double OhlcData::priceHi()
{
    return m_priceHi;
}

double OhlcData::priceLow()
{
    return m_priceLow;
}

double OhlcData::priceClose()
{
    return m_priceClose;
}

double OhlcData::timestamp()
{
    return m_timestamp;
}

double OhlcData::volume()
{
    return m_volume;
}
