#include "ordersmodel.h"

#include <QDebug>
#include "ordersmodeldetails.h"

OrdersModel::OrdersModel(QObject *parent)
    : QAbstractItemModel(parent)
    , m_selectionModel(0)
{
}

void OrdersModel::append(Order *item)
{
    int count = m_ordersList.count();

    beginInsertRows(QModelIndex(), count, count);
    m_ordersList.append(item);
    endInsertRows();

    connect(item, SIGNAL(orderInfoUpdated(Order*)), this, SLOT(orderInfoUpdated(Order*)));
}

void OrdersModel::remove(Order *item)
{
    int idx = m_ordersList.indexOf(item);

    if (idx == -1)
        return;

    beginRemoveRows(QModelIndex(), idx, idx);

    m_ordersList.removeAt(idx);
    endRemoveRows();
}

Order *OrdersModel::item(qlonglong id)
{
    int row = 0;
    Q_FOREACH (Order* order, m_ordersList)
    {
        if (order->id() == id) {
            return order;
        }
        row++;
    }
    return 0;
}

int OrdersModel::rowCount(const QModelIndex &parent) const
{
    return m_ordersList.size();
}

QVariant OrdersModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= m_ordersList.size())
        return QVariant();
    if (role == Qt::DisplayRole) {
        return QVariant::fromValue(m_ordersList.at(index.row()));
    }
    else {
        return QVariant();
    }
}

Qt::ItemFlags OrdersModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return QAbstractItemModel::flags(index) | Qt::ItemIsEnabled ;
}

QItemSelectionModel *OrdersModel::selectionModel()
{
    if (!m_selectionModel) {
        m_selectionModel = new QItemSelectionModel(this);
    }
    return m_selectionModel;
}

QVariant OrdersModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Vertical)
        return QVariant();

    if (section >= OrdersModelDetails::instance().detailsList().size())
        return QVariant();
    if (role == Qt::DisplayRole) {
        return QVariant::fromValue(OrdersModelDetails::instance().detailsList()[section]->name());
    }
    else {
        return QVariant();
    }
}

QModelIndex OrdersModel::index(int row, int column, const QModelIndex &parent) const
{
    (parent);
    if (row >= m_ordersList.size() || column >= OrdersModelDetails::instance().detailsList().size())
        return QModelIndex();
    if (row < 0 || column < 0)
        return QModelIndex();

    return createIndex(row, column);
}

QModelIndex OrdersModel::parent(const QModelIndex &child) const
{
    (child);
    return QModelIndex();
}

int OrdersModel::columnCount(const QModelIndex &parent) const
{
    (parent);
    return OrdersModelDetails::instance().detailsList().size();
}

void OrdersModel::orderInfoUpdated(Order* updatedOrder)
{
    int row = 0;
    Q_FOREACH (Order* order, m_ordersList)
    {
        if (order == updatedOrder) {
            QModelIndex idx = createIndex(row, 0, order);
            Q_EMIT dataChanged(idx, idx);
        }
        row++;
    }
}
