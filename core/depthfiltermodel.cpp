#include "depthfiltermodel.h"
#include "core/depthoffer.h"

DepthFilterModel::DepthFilterModel(QObject *parent) :
    QSortFilterProxyModel(parent),
    m_bSortedByAmount(false),
    m_bSortedByPrice(false)
{
}

bool DepthFilterModel::sortedByPrice()
{
    return m_bSortedByPrice;
}

bool DepthFilterModel::sortedByAmount()
{
    return m_bSortedByAmount;
}

double DepthFilterModel::amountMin()
{
    return m_amountMin;
}

double DepthFilterModel::amountMax()
{
    return m_amountMax;
}

double DepthFilterModel::priceMin()
{
    return m_priceMin;
}

double DepthFilterModel::priceMax()
{
    return m_priceMax;
}

void DepthFilterModel::setSortedByPrice(bool flag)
{
    m_bSortedByPrice = flag;
    filterChanged();
}

void DepthFilterModel::setSortedByAmount(bool flag)
{
    m_bSortedByAmount = flag;
    filterChanged();
}

void DepthFilterModel::setAmountMin(double amountMin)
{
    m_amountMin = amountMin;
    filterChanged();
}

void DepthFilterModel::setAmountMax(double amountMax)
{
    m_amountMax = amountMax;
    filterChanged();
}

void DepthFilterModel::setPriceMin(double priceMin)
{
    m_priceMin = priceMin;
    filterChanged();
}

void DepthFilterModel::setPriceMax(double priceMax)
{
    m_priceMax = priceMax;
    filterChanged();
}

bool DepthFilterModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    if (!m_bSortedByAmount && !m_bSortedByPrice)
        return true;

    QModelIndex index0 = sourceModel()->index(source_row, 0, source_parent);

    DepthOffer* item = index0.data().value<DepthOffer*>();

    bool bRes = true;
    if (m_bSortedByAmount)
        bRes = (item->amount() >= m_amountMin) && (item->amount() <= m_amountMax);

    if (m_bSortedByPrice)
        bRes = (item->price() >= m_priceMin) && (item->price() <= m_priceMax);

    return bRes;
}


