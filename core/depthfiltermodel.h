#ifndef DEPTHFILTERMODEL_H
#define DEPTHFILTERMODEL_H

#include <QSortFilterProxyModel>

class DepthFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit DepthFilterModel(QObject *parent = 0);
    
    bool sortedByPrice();

    bool sortedByAmount();

    double amountMin();
    double amountMax();

    double priceMin();
    double priceMax();

signals:
    
public slots:
    void setSortedByPrice(bool flag);
    void setSortedByAmount(bool flag);
    
    void setAmountMin(double amountMin);
    void setAmountMax(double amountMax);

    void setPriceMin(double priceMin);
    void setPriceMax(double priceMax);

protected:
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;

private:
    bool m_bSortedByPrice, m_bSortedByAmount;

    double m_priceMin, m_priceMax;
    double m_amountMin, m_amountMax;
};

#endif // DEPTHFILTERMODEL_H
