#include "networkmanager.h"

#include <QDebug>
#include <QDateTime>
#include <QNetworkRequest>
#include <QNetworkReply>

#include "openssl/hmac.h"
#include "api/abstractrequest.h"

NetworkManager::NetworkManager(const QString& key, const QString& secret, QObject *parent)
    : QNetworkAccessManager(parent)
    , m_key(key)
    , m_secret(secret)
    , m_sendingTimer(new QTimer(this))
    , m_nonce(0)
{
    QDateTime time;
    m_nonce = time.currentDateTime().toTime_t();

    QObject::connect(m_sendingTimer, SIGNAL(timeout()), this, SLOT(sendQueuedRequest()));
    m_sendingTimer->setInterval(2000);
    m_sendingTimer->start();
}

void NetworkManager::enqueue(AbstractRequest* request, const QString &requestString)
{
    QPair<AbstractRequest*, QString> requestInfo;
    requestInfo.first = request;
    requestInfo.second = requestString;

    qDebug() << "Request queued, " << requestString;

    m_queue.enqueue(requestInfo);
}

void NetworkManager::sendRequest(AbstractRequest *request, const QString &requestString)
{
    QString sign = hmac(requestString, m_secret);
    request->setRawHeader("Key", m_key.toAscii());
    request->setRawHeader("Sign", sign.toAscii());

    //qDebug() << Q_FUNC_INFO << "Sending: " << requestString;

    QNetworkReply* reply = post(*request, requestString.toAscii());
    reply->setReadBufferSize(102400);
    request->setReply(reply);
}

void NetworkManager::setNonce(qlonglong currentNonce)
{
    qDebug() << Q_FUNC_INFO << "Nonce updated to: " << currentNonce;
    m_nonce = currentNonce;
}

QString NetworkManager::hmac(const QString& data, const QString& secret)
{
    unsigned char digest[64];

    HMAC_CTX ctx;
    HMAC_CTX_init(&ctx);

    HMAC_Init_ex(&ctx, secret.toAscii(), secret.length(), EVP_sha512(), NULL);
    HMAC_Update(&ctx, reinterpret_cast<const unsigned char*>(data.toAscii().constData()), data.length());

    unsigned int digestLength;
    HMAC_Final(&ctx, digest, &digestLength);
    HMAC_CTX_cleanup(&ctx);

    char mdString[64*2+1];
    for (int i = 0; i < digestLength; i++)
        sprintf(&mdString[i*2], "%02x", (unsigned int)digest[i]);

    return QString::fromAscii(mdString);
}

void NetworkManager::sendQueuedRequest()
{
    int queueSize = m_queue.size();

    qDebug() << "Queue size is " << queueSize;

    if (queueSize == 0)
        return;

    QPair<AbstractRequest*,  QString> requestInfo = m_queue.dequeue();
    AbstractRequest* request = requestInfo.first;

    QString stampedRequestString = requestInfo.second + QString("&nonce=%1").arg(++m_nonce);

    QString sign = hmac(stampedRequestString, m_secret);
    request->setRawHeader("Key", m_key.toAscii());
    request->setRawHeader("Sign", sign.toAscii());

    //qDebug() << Q_FUNC_INFO << "Sending: " << stampedRequestString;

    QNetworkReply* reply = post(*request, stampedRequestString.toAscii());
    request->setReply(reply);
}
