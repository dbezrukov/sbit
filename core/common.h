#ifndef COMMON_H
#define COMMON_H

#include <QMap>
#include <QMetaType>
#include <QStringList>
#include <QVariantMap>

namespace Currency {
    const QString clr = "CLR";
    const QString btc = "BTC";
    const QString usd = "USD";
    const QString ltc = "LTC";
    const QString eur = "EUR";
    const QString rur = "RUR";

    static QStringList list(QStringList() << clr << btc << usd << ltc << eur << rur);
}

namespace TickerNames {
    const QString btcUsd = "BTC/USD";
    const QString btcEur = "BTC/EUR";
    const QString btcRur = "BTC/RUR";
    const QString ltcBtc = "LTC/BTC";
    const QString ltcUsd = "LTC/USD";
    const QString usdRur = "USD/RUR";

    static QStringList list(QStringList() << btcUsd << btcEur << btcRur << ltcBtc << ltcUsd << usdRur);
}

typedef QMap<QString, double> Funds;

Funds fundsFromJson(const QVariantMap& fundsJson);

Q_DECLARE_METATYPE(Funds)

#endif // COMMON_H
