#ifndef ORDER_H
#define ORDER_H

#include <QObject>
#include <QMetaType>

#include "common.h"

class Order : public QObject
{
    Q_OBJECT

public:
    explicit Order(qlonglong id, qlonglong timestampCreated, QObject *parent = 0);

    qlonglong id();
    qlonglong timestampCreated();

    void setTickerName(QString);
    QString tickerName();

    void setSell(bool sell);
    bool sell();

    void setamount(double amount);
    double amount();

    void setRate(double rate);
    double rate();

    void setStatus(int status);
    int status();
    QString statusString();

signals:
    void orderInfoUpdated(Order*);

protected:
    qlonglong m_id;
    QString m_tickerName;
    bool m_sell;
    double m_amount;
    double m_rate;
    qlonglong m_timestampCreated;
    int m_status;
};

Q_DECLARE_METATYPE(Order*)
Q_DECLARE_METATYPE(QList<Order*>)

#endif // ORDER_H
