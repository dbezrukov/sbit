#include "common.h"

Funds fundsFromJson(const QVariantMap& fundsJson)
{
    Funds funds;
    funds[Currency::btc] = fundsJson["btc"].toDouble();
    funds[Currency::ltc] = fundsJson["ltc"].toDouble();
    funds[Currency::clr] = fundsJson["clr"].toDouble();
    funds[Currency::usd] = fundsJson["usd"].toDouble();
    funds[Currency::eur] = fundsJson["eur"].toDouble();
    funds[Currency::rur] = fundsJson["rur"].toDouble();
    return funds;
}
