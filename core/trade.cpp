#include "trade.h"

Trade::Trade(qlonglong id, qlonglong date, QObject *parent) :
    QObject(parent),
    m_id(id),
    m_date(date),
    m_price(0),
    m_amount(0),
    m_sell(false)
{
}

qlonglong Trade::id()
{
    return m_id;
}

qlonglong Trade::date()
{
    return m_date;
}

double Trade::price()
{
    return m_price;
}

void Trade::setPrice(double price)
{
    m_price = price;
}

double Trade::amount()
{
    return m_amount;
}

void Trade::setAmount(double amount)
{
    m_amount = amount;
}

void Trade::setSell(bool sell)
{
    m_sell = sell;
}

bool Trade::sell()
{
    return m_sell;
}
