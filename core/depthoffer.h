#ifndef DEPTHOFFER_H
#define DEPTHOFFER_H

#include <QObject>
#include <QMetaType>

class DepthOffer : public QObject
{
    Q_OBJECT
public:
    explicit DepthOffer(QObject *parent = 0);

    double price();
    void setPrice(double price);

    double amount();
    void setAmount(double amount);

    bool sell();
    void setSell(bool sell);

    bool operator==(const DepthOffer& rh);
    bool operator<(const DepthOffer& rh);

    bool IsInList(const QList<DepthOffer*>& offersList);
    bool IsInList(const QList<DepthOffer*>& offersList, int maxCheckIndex);
    int IndexInList(const QList<DepthOffer*>& offersList);
signals:

public slots:

protected:
    double m_price;
    double m_amount;
    bool m_sell;
};

template <class T>
class PCompareByVal
{
    explicit PCompareByVal(T* pT);
    T* getPointer();
    T& operator->();
    bool operator==(const PCompareByVal& rh);
private:
    T* m_pT;
};

Q_DECLARE_METATYPE(DepthOffer*)
//Q_DECLARE_METATYPE(QList<DepthOffer*>)

#endif // DEPTHOFFER_H
