#include "chartsmodel.h"

#include <QDebug>

ChartsModel::ChartsModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_selectionModel(0)
    , m_ohlcIteraor(NULL)
{
}

void ChartsModel::append(OhlcData *item)
{
    int count = m_ohlcDataList.count();

    beginInsertRows(QModelIndex(), count, count);
    m_ohlcDataList.append(item);
    endInsertRows();

    connect(item, SIGNAL(ohlcInfoUpdated(OhlcData*)), this, SLOT(ohlcInfoUpdated(OhlcData*)));
}

void ChartsModel::remove(OhlcData *item)
{
    int idx = m_ohlcDataList.indexOf(item);

    if (idx == -1)
        return;

    beginRemoveRows(QModelIndex(), idx, idx);

    m_ohlcDataList.removeAt(idx);
    endRemoveRows();
}

OhlcData *ChartsModel::item(qlonglong timestamp)
{
    int row = 0;
    Q_FOREACH (OhlcData* ohlc, m_ohlcDataList)
    {
        if (ohlc->timestamp() == timestamp) {
            return ohlc;
        }
        row++;
    }
    return 0;
}

int ChartsModel::rowCount(const QModelIndex &parent) const
{
    return m_ohlcDataList.size();
}

QVariant ChartsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= m_ohlcDataList.size())
        return QVariant();
    if (role == Qt::DisplayRole) {
        return QVariant::fromValue(m_ohlcDataList.at(index.row()));
    }
    else {
        return QVariant();
    }
}

Qt::ItemFlags ChartsModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return QAbstractItemModel::flags(index) | Qt::ItemIsEnabled ;
}

QItemSelectionModel *ChartsModel::selectionModel()
{
    if (!m_selectionModel) {
        m_selectionModel = new QItemSelectionModel(this);
    }
    return m_selectionModel;
}

QListIterator<OhlcData *>* ChartsModel::iterator()
{
    if (m_ohlcIteraor) delete m_ohlcIteraor;

    m_ohlcIteraor = new QListIterator<OhlcData*>(m_ohlcDataList);

    return m_ohlcIteraor;
}

int ChartsModel::size()
{
    return m_ohlcDataList.size();
}

void ChartsModel::ohlcInfoUpdated(OhlcData* updatedOhlc)
{
    int row = 0;
    Q_FOREACH (OhlcData* ohlc, m_ohlcDataList)
    {
        if (ohlc == updatedOhlc) {
            QModelIndex idx = createIndex(row, 0, ohlc);
            Q_EMIT dataChanged(idx, idx);
        }
        row++;
    }
}
