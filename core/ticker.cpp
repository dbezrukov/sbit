#include "ticker.h"

Ticker::Ticker(QString name, QObject *parent)
    : QObject(parent)
    , m_name(name)
    , m_price(0.f)
{
}

QString Ticker::name()
{
    return m_name;
}

void Ticker::setPrice(double price)
{
    m_price = price;
    emit tickerInfoUpdated(this);
}

double Ticker::price()
{
    return m_price;
}
