#ifndef TICKER_H
#define TICKER_H

#include <QObject>
#include <QMetaType>

#include "common.h"

class Ticker : public QObject
{
    Q_OBJECT

public:
    explicit Ticker(QString name, QObject *parent = 0);

    QString name();

    void setPrice(double price);
    double price();

signals:
    void tickerInfoUpdated(Ticker*);

protected:
    QString m_name;
    double m_price;
};

Q_DECLARE_METATYPE(Ticker*)

#endif // TICKER_H
