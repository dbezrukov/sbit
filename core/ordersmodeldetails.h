#ifndef ORDERSMODELDETAILS_H
#define ORDERSMODELDETAILS_H

#include "common.h"
#include "ui/orderitemwidgets.h"

class OrderDetail
{
public:
    OrderDetail(const QString& name);
    const QString& name();
    void setName(QString name);
    virtual OrderItemWidget& orderItemWidget() = 0;
protected:
    QString m_name;
};

class ActionOrderDetail : public OrderDetail
{
public:
    ActionOrderDetail();
    virtual OrderItemWidget& orderItemWidget();
private:
    ActionOrderItemWidget widget;
};

class PairOrderDetail : public OrderDetail
{
public:
    PairOrderDetail();
    virtual OrderItemWidget& orderItemWidget();
private:
    PairOrderItemWidget widget;
};

class PriceOrderDetail : public OrderDetail
{
public:
    PriceOrderDetail();
    virtual OrderItemWidget& orderItemWidget();
private:
    PriceOrderItemWidget widget;
};

class QntyOrderDetail : public OrderDetail
{
public:
    QntyOrderDetail();
    virtual OrderItemWidget& orderItemWidget();
private:
    QntyOrderItemWidget widget;
};

class SummOrderDetail : public OrderDetail
{
public:
    SummOrderDetail();
    virtual OrderItemWidget& orderItemWidget();
private:
    SummOrderItemWidget widget;
};

class StatusOrderDetail : public OrderDetail
{
public:
    StatusOrderDetail();
    virtual OrderItemWidget& orderItemWidget();
private:
    StatusOrderItemWidget widget;
};

class OrdersModelDetails
{
public:
    static OrdersModelDetails& instance();
    QList<OrderDetail*>& detailsList();

private:
    OrdersModelDetails();
    ~OrdersModelDetails();
    QList<OrderDetail*> m_details;
};

#endif // ORDERSMODELDETAILS_H
