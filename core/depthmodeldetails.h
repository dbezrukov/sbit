#ifndef DEPTHMODELDETAILS_H
#define DEPTHMODELDETAILS_H

#include "common.h"
#include "ui/depthitemwidgets.h"

class DepthDetail
{
public:
    DepthDetail(const QString& name);
    const QString& name();
    void setName(QString name);
    virtual DepthItemWidget& depthItemWidget() = 0;
protected:
    QString m_name;
};

class SaleDepthDetail : public DepthDetail
{
public:
    SaleDepthDetail();
    virtual DepthItemWidget& depthItemWidget();
private:
    SaleDepthItemWidget widget;
};

class PriceDepthDetail : public DepthDetail
{
public:
    PriceDepthDetail();
    virtual DepthItemWidget& depthItemWidget();
private:
    PriceDepthItemWidget widget;
};

class PurchaseDepthDetail : public DepthDetail
{
public:
    PurchaseDepthDetail();
    virtual DepthItemWidget& depthItemWidget();
private:
    PurchaseDepthItemWidget widget;
};

class DepthModelDetails
{
public:
    static DepthModelDetails& instance();
    QList<DepthDetail*>& detailsList();

private:
    DepthModelDetails();
    ~DepthModelDetails();
    QList<DepthDetail*> m_details;
};
#endif // DEPTHMODELDETAILS_H
