#ifndef TRADE_H
#define TRADE_H

#include <QObject>
#include <QMetaType>

class Trade : public QObject
{
    Q_OBJECT
public:
    explicit Trade(qlonglong id, qlonglong date, QObject *parent = 0);
    
    qlonglong id();
    qlonglong date();

    double price();
    void setPrice(double price);

    double amount();
    void setAmount(double amount);

    void setSell(bool sell);
    bool sell();
signals:
    
public slots:
    
protected:
    qlonglong m_date;
    qlonglong m_id;

    double m_price;
    double m_amount;
    bool m_sell;
};

Q_DECLARE_METATYPE(Trade*)

#endif // TRADE_H
