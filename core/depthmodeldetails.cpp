#include "depthmodeldetails.h"

DepthDetail::DepthDetail(const QString& name) :
    m_name(name)
{
}

const QString &DepthDetail::name()
{
    return m_name;
}

void DepthDetail::setName(QString name)
{
    m_name = name;
}

SaleDepthDetail::SaleDepthDetail() :
    DepthDetail(QObject::trUtf8("Продажа"))
{

}

DepthItemWidget &SaleDepthDetail::depthItemWidget()
{
    return widget;
}


PriceDepthDetail::PriceDepthDetail() :
        DepthDetail(QObject::trUtf8("Цена"))
{
}

DepthItemWidget &PriceDepthDetail::depthItemWidget()
{
    return widget;
}


PurchaseDepthDetail::PurchaseDepthDetail() :
    DepthDetail(QObject::trUtf8("Покупка"))
{
}

DepthItemWidget &PurchaseDepthDetail::depthItemWidget()
{
    return widget;
}


DepthModelDetails &DepthModelDetails::instance()
{
    static DepthModelDetails tmd;
    return tmd;
}

QList<DepthDetail *> &DepthModelDetails::detailsList()
{
    return m_details;
}

DepthModelDetails::DepthModelDetails()
{
    m_details.append(new SaleDepthDetail());
    m_details.append(new PriceDepthDetail());
    m_details.append(new PurchaseDepthDetail());
}

DepthModelDetails::~DepthModelDetails()
{
    while (m_details.size())
    {
        delete m_details.front();
        m_details.pop_front();
    }
}
