#ifndef CHARTSMODEL_H
#define CHARTSMODEL_H

#include <QAbstractListModel>
#include <QItemSelectionModel>
#include <QMap>

#include "common.h"
#include "ohlcdata.h"

class ChartsModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ChartsModel(QObject *parent = 0);

    void append(OhlcData* item);
    void remove(OhlcData* item);
    OhlcData* item(qlonglong timestamp);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QItemSelectionModel *selectionModel();
    QListIterator<OhlcData *> *iterator();
    int size();

protected slots:
    void ohlcInfoUpdated(OhlcData*);

private:
    QList<OhlcData*> m_ohlcDataList;
    QListIterator<OhlcData*> *m_ohlcIteraor;
    QItemSelectionModel* m_selectionModel;
};

#endif // CHARTSMODEL_H
