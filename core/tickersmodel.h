#ifndef TICKERSMODEL_H
#define TICKERSMODEL_H

#include <QAbstractListModel>
#include <QItemSelectionModel>
#include <QMap>

#include "common.h"
#include "ticker.h"

class TickersModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit TickersModel(QObject *parent = 0);
    void append(Ticker* item);
    Ticker* item(QString);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QItemSelectionModel *selectionModel();

protected slots:
    void tickerInfoUpdated(Ticker*);

private:
    QMap<int, Ticker*> map;
    QItemSelectionModel* m_selectionModel;
};

#endif // TICKERSMODEL_H
