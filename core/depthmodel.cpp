#include "depthmodel.h"

#include <QDebug>
#include "depthmodeldetails.h"

DepthModel::DepthModel(QObject *parent) :
    QAbstractItemModel(parent),
    m_selectionModel(NULL)
{

}

DepthModel::~DepthModel()
{

}

int DepthModel::rowCount(const QModelIndex &parent) const
{
    (parent);
    return m_listByRow.count();
}

QVariant DepthModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= m_listByRow.size() || index.column() >= DepthModelDetails::instance().detailsList().size())
        return QVariant();
    if (role == Qt::DisplayRole) {
        return QVariant::fromValue(m_listByRow[index.row()]);
    }
    else {
        return QVariant();
    }
}

Qt::ItemFlags DepthModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return QAbstractItemModel::flags(index) | Qt::ItemIsEnabled ;
}

QItemSelectionModel *DepthModel::selectionModel()
{
    if (!m_selectionModel) {
        m_selectionModel = new QItemSelectionModel(this);
    }
    return m_selectionModel;
}

QVariant DepthModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Vertical)
        return QVariant();

    if (section >= DepthModelDetails::instance().detailsList().size())
        return QVariant();
    if (role == Qt::DisplayRole) {
        return QVariant::fromValue(DepthModelDetails::instance().detailsList()[section]->name());
    }
    else {
        return QVariant();
    }
}

QModelIndex DepthModel::index(int row, int column, const QModelIndex &parent) const
{
    (parent);
    if (row >= m_listByRow.size() || column >= DepthModelDetails::instance().detailsList().size())
        return QModelIndex();
    if (row < 0 || column < 0)
        return QModelIndex();

    return createIndex(row, column);
}

QModelIndex DepthModel::parent(const QModelIndex &child) const
{
    (child);
    return QModelIndex();
}

int DepthModel::columnCount(const QModelIndex &parent) const
{
    (parent);
    return DepthModelDetails::instance().detailsList().size();
}

void DepthModel::remove(DepthOffer *pDepthOffer)
{
    int idx = pDepthOffer->IndexInList(m_listByRow);

    if (idx == -1)
        return;

    beginRemoveRows(QModelIndex(), idx, idx);

    delete m_listByRow.at(idx);
    m_listByRow.removeAt(idx);
    endRemoveRows();
}

void DepthModel::append(DepthOffer *pDepthOffer)
{
    // bisection method
    int idx = 0;
    if (m_listByRow.count())
    {
        int leftInd = 0;
        int rightInd = m_listByRow.count() - 1;
        while (leftInd != rightInd)
        {
            if (rightInd - leftInd == 1)
            {
                DepthOffer* pRightCandOffer = m_listByRow.at(rightInd);

                if (*pRightCandOffer < *pDepthOffer)
                    idx = rightInd + 1;
                break;
            }
            int candInd = leftInd + (rightInd - leftInd) / 2;
            DepthOffer* pCandOffer = m_listByRow.at(candInd);
            if (*pCandOffer < *pDepthOffer)
                leftInd = candInd;
            else
                rightInd = candInd;
        }

        if (!idx)
        {
            DepthOffer* pCandOffer = m_listByRow.at(leftInd);
            if (*pCandOffer < *pDepthOffer)
                idx = leftInd + 1;
            else
                idx = leftInd;
        }
    }
    beginInsertRows(QModelIndex(), idx, idx);
    if (idx == m_listByRow.count())
        m_listByRow.append(pDepthOffer);
    else
        m_listByRow.insert(idx, pDepthOffer);
    endInsertRows();
}

void DepthModel::setDepth(QList<DepthOffer*> depthList)
{
    foreach (DepthOffer* depthOffer, m_listByRow)
    {
        if (!depthOffer->IsInList(depthList))
            remove(depthOffer);
    }

    foreach (DepthOffer* depthOffer, depthList)
    {
        if (!depthOffer->IsInList(m_listByRow))
            append(depthOffer);
    }
}
