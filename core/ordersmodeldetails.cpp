#include "ordersmodeldetails.h"

OrderDetail::OrderDetail(const QString& name) :
    m_name(name)
{
}

const QString &OrderDetail::name()
{
    return m_name;
}

void OrderDetail::setName(QString name)
{
    m_name = name;
}

ActionOrderDetail::ActionOrderDetail() :
    OrderDetail(QObject::trUtf8("Действие"))
{

}

OrderItemWidget &ActionOrderDetail::orderItemWidget()
{
    return widget;
}


PairOrderDetail::PairOrderDetail() :
       OrderDetail(QObject::trUtf8("Пара"))
{
}

OrderItemWidget &PairOrderDetail::orderItemWidget()
{
    return widget;
}


PriceOrderDetail::PriceOrderDetail() :
    OrderDetail(QObject::trUtf8("Цена"))
{
}

OrderItemWidget &PriceOrderDetail::orderItemWidget()
{
    return widget;
}

QntyOrderDetail::QntyOrderDetail() :
    OrderDetail(QObject::trUtf8("Количество"))
{
}

OrderItemWidget &QntyOrderDetail::orderItemWidget()
{
    return widget;
}

SummOrderDetail::SummOrderDetail() :
    OrderDetail(QObject::trUtf8("Сумма"))
{
}

OrderItemWidget &SummOrderDetail::orderItemWidget()
{
    return widget;
}

StatusOrderDetail::StatusOrderDetail() :
    OrderDetail(QObject::trUtf8("Статус"))
{
}

OrderItemWidget &StatusOrderDetail::orderItemWidget()
{
    return widget;
}

OrdersModelDetails &OrdersModelDetails::instance()
{
    static OrdersModelDetails tmd;
    return tmd;
}

QList<OrderDetail *> &OrdersModelDetails::detailsList()
{
    return m_details;
}

OrdersModelDetails::OrdersModelDetails()
{
    m_details.append(new ActionOrderDetail());
    m_details.append(new PairOrderDetail());
    m_details.append(new PriceOrderDetail());
    m_details.append(new QntyOrderDetail());
    m_details.append(new SummOrderDetail());
    m_details.append(new StatusOrderDetail());
}

OrdersModelDetails::~OrdersModelDetails()
{
    while (m_details.size())
    {
        delete m_details.front();
        m_details.pop_front();
    }
}















