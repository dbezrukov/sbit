#include "tickersmodel.h"

TickersModel::TickersModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_selectionModel(0)
{
}

void TickersModel::append(Ticker *item)
{
    map.insert(map.size(), item);
    connect(item, SIGNAL(tickerInfoUpdated(Ticker*)), this, SLOT(tickerInfoUpdated(Ticker*)));
}

Ticker *TickersModel::item(QString name)
{
    foreach (Ticker* ticker, map) {
        if (ticker->name() == name) {
            return ticker;
        }
    }
    return 0;
}

int TickersModel::rowCount(const QModelIndex &parent) const
{
    return map.size();
}

QVariant TickersModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= map.size())
        return QVariant();
    if (role == Qt::DisplayRole) {
        return QVariant::fromValue(map[index.row()]);
    }
    else {
        return QVariant();
    }
}

Qt::ItemFlags TickersModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return QAbstractItemModel::flags(index) | Qt::ItemIsEnabled ;
}

QItemSelectionModel *TickersModel::selectionModel()
{
    if (!m_selectionModel) {
        m_selectionModel = new QItemSelectionModel(this);
    }
    return m_selectionModel;
}

void TickersModel::tickerInfoUpdated(Ticker* ticker)
{
    foreach (int key, map.keys()) {
        if (map.value(key) == ticker) {
            QModelIndex idx = createIndex(key, 0, ticker);
            Q_EMIT dataChanged(idx, idx);
        }
    }
}
