#ifndef ORDERSMODEL_H
#define ORDERSMODEL_H

#include <QAbstractListModel>
#include <QItemSelectionModel>
#include <QMap>

#include "common.h"
#include "order.h"

class OrdersModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit OrdersModel(QObject *parent = 0);

    void append(Order* item);
    void remove(Order* item);
    Order* item(qlonglong id);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QItemSelectionModel *selectionModel();

    virtual QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const;
    virtual QModelIndex index(int row, int column,
                              const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &child) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;

protected slots:
    void orderInfoUpdated(Order*);

private:
    QList<Order*> m_ordersList;
    QItemSelectionModel* m_selectionModel;
};
#endif // ORDERSMODEL_H
