#ifndef DEPTHMODEL_H
#define DEPTHMODEL_H

#include <QAbstractListModel>
#include <QItemSelectionModel>
#include <QMap>
#include <QList>

#include "common.h"
#include "depthoffer.h"

class DepthModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit DepthModel(QObject *parent = 0);
    ~DepthModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QItemSelectionModel *selectionModel();
    virtual QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const;
    virtual QModelIndex index(int row, int column,
                              const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &child) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;

    // append, remove
    void remove(DepthOffer* pDepthOffer);
    void append(DepthOffer* pDepthOffer);
    void setDepth(QList<DepthOffer*> depthList);


signals:

public slots:

private:
    QList<DepthOffer*> m_listByRow;
    QItemSelectionModel* m_selectionModel;
};

#endif // DEPTHMODEL_H
